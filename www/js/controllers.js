angular.module('starter.controllers', [])
  .controller('AppCtrl', function ($ionicModal, $scope, $stateParams,$ionicPopup,$ionicHistory,location,$ionicViewService, $interval, $rootScope, location, $ionicLoading, $timeout, $state, ajax_service, fb) {

    $scope.tracking = function () {
      // BackgroundGeolocationService.init(type);
      location.get_coords().then(function (res) {
        if (res.status == "success") {
          coords_data = res;
          if ($scope.user.admin_role_id == 9 || $scope.user.admin_role_id == 6 || $scope.user.admin_role_id == 5) {
            fb.set('rapro/' + $scope.user.id + '/' + coords_data.date + '/current_location', coords_data.data);
          }
        }
      });
    };
    $scope.login_data = {};
    $scope.img_url = img_url;
    $scope.start_interval = function (type) {

      $scope.track_interval = $interval($scope.tracking, interval_duration);

    };
    $scope.stop_interval = function () {
      $interval.cancel(track_interval);
      // BackgroundGeolocationService.stop();
    };
    $rootScope.$on('user-data', function(event, args) {
    console.log(args.user_data);
    $scope.user = args.user_data
    });
    if (localStorage.getItem('rapro')) {
      $scope.user = JSON.parse(localStorage.getItem('rapro'));
      if ($scope.user.admin_role_id == 9 || $scope.user.admin_role_id == 6 || $scope.user.admin_role_id == 5) {
        $scope.start_interval();
      }
    }
    $scope.login = function (data) {
      $scope.user = {};
      $scope.data = {};
      $scope.data.user_id =data.user_id;
      $scope.data.password =data.password;
      $scope.data.token = token;
      $scope.data.device_token = JSON.parse(localStorage.getItem('AppToken'));
      ajax_service.post_obj(url + 'login', $scope.data).then(function (result) {
        if (result.status == 'success') {
          $scope.login_data = {};
          user = result.data;
          $scope.user = result.data;
          localStorage.setItem('rapro', JSON.stringify(user));
          if ((user.admin_role_id == 4) || (user.admin_role_id == 7)) {
            $state.go('app.welcome');
           } else {
            $state.go('app.home_page');
           if (user.admin_role_id == 9 || user.admin_role_id == 6 || user.admin_role_id == 5) {
             // $scope.start_interval("login");
              location.get_coords().then(function (res) {

                if (res.status == "success") {
                  coords_data = res;
                  fb.push('rapro/' + user.id + '/' + coords_data.date + '/login_coords', coords_data.data, 'login');
                }
              });

            }
          }
        } else {
          $ionicPopup.alert({
            template: result.message
          });
        }
      });
    };
    $scope.logout = function () {
      // $ionicLoading.show();
      $scope.user = {};
      $scope.user.token = token;
      $scope.user.user_id = user.id;
      ajax_service.post_obj(url + 'logout', $scope.user).then(function (result) {
        if (result.status == 'success') {
          if (  user.admin_role_id == 9 ||   user.admin_role_id == 6  ||   user.admin_role_id == 5) {
            location.get_coords().then(function (res) {
              coords_data = res;
              fb.push('rapro/' + user.id + '/' + coords_data.date + '/logout_coords', coords_data.data, 'logout');
              $scope.stop_interval();
              $scope.user = {};
              user = {};
              localStorage.removeItem('rapro');
              $state.go('menu_welcome');
            });
            $scope.user = {};
             user = {};
             localStorage.removeItem('rapro');
            $state.go('menu_welcome');
          } else {
           $scope.user = {};
            user = {};
            localStorage.removeItem('rapro');
            $state.go('menu_welcome');
          }
        }
      });
    };
    $scope.$on('$destroy', function () {
      $scope.stop_interval();
    });
    category_list = function () {
      category_data = {};
      category_data.token = token;
      ajax_service.post_obj(url + 'issue_categories', category_data).then(function (result) {
        if (result.status == "success") {
          $scope.categories = result.data;
        }
      });
    };
    category_list();
    home_data = function (user_data) {
      user_data.token = token;
      ajax_service.post_obj(url + 'home_page_data', user_data).then(function (result) {
        if (result.status == "success") {
          $scope.home_data = result.data;
        }
      });
    };
    $scope.showRating = function () {
      myPopup = $ionicPopup.show({
        title: 'Status',
        template: '<div class="text-center">Work Completed Notification</div><div class="text-center padding"> <rating ng-model="rating.rate" max="rating.max"></rating></div><button class="button button-positive button-small hed_butn" >Satisfied</button><button class="button button-positive button-small hed_butn " ng-click="close()">Not Satisfied</button>',
        scope: $scope,
      });
    };

    $rootScope.$on('scanner-started', function(event, args) {
console.log(args.data);
$scope.notifications_count = args.data
});
 $scope.notifications_list = function(){
         data = {};
        data.token = token;
        data.user_id = user.id;
         ajax_service.post_obj(url + 'notifications', data).then(function (result) {
          if (result.status == 'success') {
            console.log(result);
            $scope.notifications = result.data;
         $scope.notifications_count = result.count;

          }
        });

       };

       $scope.fun=function(id){
        data={};
             data.id=id;
          data.user_id = user.id;
            ajax_service.post_obj(url + 'notifications_status', data).then(function (result) {
              if (result.status == 'success') {
                console.log(result);
                $scope.notifications_count = result.count;
                $rootScope.$broadcast('scanner-started', { data: result.count });

              }
            });
       }
       $scope.notifications_list();
  })
  .controller('home_ctrl', function ($scope, $ionicPopup,$ionicHistory,$ionicViewService,location, $interval,$rootScope, location, $ionicLoading, $timeout, $state, ajax_service, fb) {
  $scope.user_id=user.id;
    $scope.close = function () {
      myPopup.close();
    };
    $scope.att_pin={};
    if (localStorage.getItem('rapro')) {
      $scope.user = JSON.parse(localStorage.getItem('rapro'));
        $rootScope.$broadcast('user-data', { user_data: $scope.user });
      if ($scope.user.admin_role_id == 9 || $scope.user.admin_role_id == 6 || $scope.user.admin_role_id == 5) {
        // BackgroundGeolocationService.init("login");
        $scope.start_interval("login");
      }
    }

    $scope.attendance = function (id) {
      data = {};
      data.token = token;
      data.user_id = user.id;
      data.id = id;
      console.log($scope.att_pin);
      ajax_service.post_obj(url + 'attendance_store', data).then(function (result) {
        console.log(result);
        $scope.att_pin={};
        if (result.status == 'success') {
          $scope.attendance_check();
          myPopup = $ionicPopup.show({
            title: result.message
          });
        } else {
          myPopup = $ionicPopup.show({
            title: result.message
          });
        }

        $timeout(function () {
          myPopup.close();
        }, 2000);
      });
    };

    $scope.rating = {};
    $scope.rating.rate = 3;
    $scope.rating.max = 5;
$scope.attendance_check=function(){
  data = {};
  data.token = token;
  data.user_id = user.id;
  ajax_service.post_obj(url + 'attendance_check', data).then(function (result) {
    console.log(result.message);
$scope.message_content=result.message;
  });
}
$scope.attendance_check();
$ionicHistory.clearHistory();
  })
  .controller('MyCtrl', function ($scope,$ionicHistory,$ionicViewService, $ionicPopover, $state) {
$ionicHistory.clearHistory();
 })
  .controller('issue_create_ctrl', function ($stateParams,modal_service,$cordovaFile, $scope, $ionicPopover, camera_actions, fb, $state,$ionicLoading, ajax_service, $ionicModal, $ionicSlideBoxDelegate, $ionicPopup, $timeout, $ionicActionSheet, $cordovaCapture, $ionicScrollDelegate) {
    $scope.title_new="Register Issue";
     $scope.rm=true;
     $scope.btn="Submit";
    const update_id = $stateParams.id;
    if (user.admin_role_id == 5 || user.admin_role_id == 6) {
      $scope.buttonshow = true;
    } else {
      $scope.buttonshow = false;
      $scope.buttonhide = true;
    }

    $ionicPopover.fromTemplateUrl('templates/issuebtns.html', {
      scope: $scope,
    }).then(function (popover) {
      $scope.popover = popover;
    });
    $scope.doSomething = function ($event) {
      $scope.popover.show($event);
    };
    $scope.gobuttons = function (norms) {
      if (norms == 1) {
        $state.go('app.create_issue');
        $scope.popover.hide();
      } else {
        $state.go('app.issue_status');
        $scope.popover.hide();
      }
    };
    $scope.zoomMin = 1;
    $scope.showImages = function (index, images) {
      $scope.activeSlide = index;
      $scope.images = images;
      options = {
        scope: $scope
      };
      modal_service.from_url('templates/type.html', options).then(function (modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    };
    $scope.closeModal = function () {
      $scope.modal.hide();
      $scope.modal.remove();
    };

    $scope.property = function (data) {
      issue_data = {};
      issue_data.token = token;
      issue_data.user_id = user.id;
      ajax_service.post_obj(url + 'property_name', issue_data).then(function (result) {
        console.log(result);
        $scope.property_name_list = result;
      });
    };
    $scope.property();

    $scope.property_show = function () {
      data = {};
      data.token = token;
      data.user_id = user.id;
      ajax_service.post_obj(url + 'property_name', data).then(function (result) {
        console.log(result);
          $scope.property_name_list = result;
      });
      options = {
        scope: $scope
      };
      modal_service.from_url('templates/property_show.html', options).then(function (res) {
        $scope.modal = res;
        console.log(res);
        $scope.modal.show();
      });
    };

    $scope.property_detail_single=function(data){

      console.log(data);
      $scope.bind=data;
      $scope.closeModal();
    }


    $scope.image_a = [];
    $scope.img_data = [];
    $scope.showActionsheet = function (multi_issues, index_value) {
      $ionicActionSheet.show({
        titleText: 'ActionSheet Example',
        buttons: [{
          text: '<i class="icon ion-camera "></i> Camera'
        },
        {
          text: '<i class="icon ion-android-image"></i> Gallery'
        },
        ],
        cancelText: 'Cancel',
        cancel: function () { },
        buttonClicked: function (index) {
          if (index == 0) {
            $scope.getPicture('camera', multi_issues, index_value);
          }
          if (index == 1) {
            $scope.getPicture('file', multi_issues, index_value);
          }
          return true;
        },
      });
    };
    $scope.getPicture = function (type, multi_issues, index_value) {
      camera_actions.selImages(type).then(function (imageUri) {
        if (imageUri != undefined) {

          $scope.multi_issues[index_value].image.push(imageUri);
        }
      });
    };
    $scope.remove = function (parent, child, item) {
      $scope.multi_issues[parent].image.splice(child, 1);
    };
    $scope.video_a = '';
    $scope.captureVideo = function () {
      $cordovaCapture.captureVideo(video_option).then(function (videoData) {
        $scope.video_a = videoData[0].fullPath;
      }, function (err) {
        alert(JSON.stringify(err.message));
      });
    };

    $scope.issue = {};
    $scope.issue_create = function (issue, multi_issues, videos,propertyid) {
      console.log(issue);
      issue.token = token;
      issue.user_id = user.id;
      issue.property_id=propertyid;
      console.log(issue);

        if (videos) {
          vid_params = {
            path: 'issue',
            type: 'videos',
          };
          camera_actions.file_upload(url + 'upload_file', videos, vid_params).then(function (res) {
            if (res.status == 'success') {
              issue.video = res.response;
              issue_store_process(issue, multi_issues);
            }

          });
        } else {
            issue_store_process(issue,multi_issues);
        }
    };

    var issue_store_process = function (issue, multi_issues) {


      img_params = {
        path: 'issue',
        type: 'photos',
      };
      file_ids = [];
      $scope.newData = [];
      angular.forEach(multi_issues, function (value1,key1) {
             $scope.newData.push(value1);
if (value1.image.length > 0){
        value1.images = [];
           angular.forEach(value1.image, function (value,key) {
     var a = value.search("http");
          if(a != 0){
                camera_actions.file_upload(url + 'upload_file', value, img_params).then(function (res) {

                if (res.status == 'success') {
                  value1.images.push(res.response);
                  if (value1.image.length - 1 == key) {
                    if (multi_issues.length - 1 == key1) {
                      issue_store(issue, multi_issues);
                    }
                  }
                }
              });
            }
        });
      } else {
        if ((multi_issues.length - 1 == key1) && value1.image.length == 0) {
            $timeout(function() {
              issue_store(issue, multi_issues);
            },3000);
        }
      }
      });
    };

    var issue_store = function (issue, multi_issues) {

      issue.details = multi_issues;
      ajax_service.post_obj(url + 'issue_store', issue).then(function (response) {
        if (response.status == 'success') {
          $scope.issue = {};
          $scope.image_a = [];
          $scope.video_a = '';
          $scope.multi_issues=[];
          myPopup = $ionicPopup.show({
            title: '<p class="balanced">Issue Registered Successfully</p>'
          });
          $timeout(function () {
            myPopup.close();
            $state.go('app.home_page');
          }, 3000);
        } else {
          myPopup = $ionicPopup.show({
            title: '<p class="assertive">Please fill all mandatory fields</p>'
          });
          $scope.message = response.message;
          $scope.msgs = 'assertive';
          $scope.validation = {};
          $scope.err = {};
          $scope.err_word = {};
          angular.forEach(response.errors, function (value, key) {
            $scope.validation[key] = value[1];
            $scope.err[key] = 'red_border';
            $scope.err_word[key] = value[0];
          });
          $timeout(function () {
            myPopup.close();
            $scope.msgs = "";
          }, 3000);
        }
      });
    };

    $scope.reset_all = function (add_quotation) {
      $scope.multi_issues = [{
        'category_id': "",
        'image': [],
        'description': "",
      }];
    }
    $scope.multi_issues = [{
      'category_id': "",
      'image': [],
      'description': ""

    }];

    $scope.addNew = function (add) {
      $scope.multi_issues.push({
        'category_id': $scope.category_id,
        'image': [],
        'description': $scope.description,
      });
    };
    $scope.remove_m = function () {
      var newDataList = [];
      $scope.selectedAll = false;
      angular.forEach($scope.multi_issues, function (selected) {
        if (!selected.selected) {
          newDataList.push(selected);
        }
      });
      $scope.multi_issues = newDataList;
    };

    $scope.checkAll = function () {
      if (!$scope.selectedAll) {
        $scope.selectedAll = true;
      } else {
        $scope.selectedAll = false;
      }
      angular.forEach($scope.multi_issues, function (quotation) {
        quotation.selected = $scope.selectedAll;
      });
    };
    $scope.edit_pro = false;
    $scope.update_issue=function(update_id){
      issue_data = {};
      issue_data.token = token;
      issue_data.id = update_id;
      issue_data.user_id = user.id;
      console.log(issue_data);
      ajax_service.post_obj(url + 'edit_issue', issue_data).then(function (response) {
        console.log(response);
        if (response.status == "success") {
          $scope.title_new="Update Issue";
          $scope.btn="Update";
          $scope.rm=false;
          $scope.edit_pro =true;
          $scope.issue = response.data;
          $scope.multi_issues = $scope.issue.details;
          angular.forEach($scope.multi_issues, function (value, key) {
          img_array = [];
          angular.forEach(value.image_list, function (value_i, key_i) {
            if (value != null) {
              img_array.push(img_url + value_i.image_path + '/' + value_i.image_name);
            }
          });
          value.image = img_array;
          });

      }
      });
    };
    if(update_id){
    $scope.update_issue(update_id)
    }
  })

  .controller('issue_ctrl', function ($stateParams, $scope, $ionicPopover, camera_actions, fb, $state, modal_service, $ionicLoading, ajax_service, $ionicModal, $ionicSlideBoxDelegate, $ionicPopup, $timeout, $ionicActionSheet, $cordovaCapture, $ionicScrollDelegate) {
    var issue_id = $stateParams.issue_id;
    if (user.admin_role_id == 5 || user.admin_role_id == 6) {
      $scope.buttonshow = true;
    } else {
      $scope.buttonshow = false;
      $scope.buttonhide = true;
    }

    $ionicPopover.fromTemplateUrl('templates/issuebtns.html', {
      scope: $scope,
    }).then(function (popover) {
      $scope.popover = popover;
    });
    $scope.doSomething = function ($event) {
      $scope.popover.show($event);
    };
    $scope.gobuttons = function (norms) {
      if (norms == 1) {
        $state.go('app.create_issue');
        $scope.popover.hide();
      } else {
        $state.go('app.issue_status');
        $scope.popover.hide();
      }
    };
    $scope.zoomMin = 1;
    $scope.showImages = function (index, images) {
      $scope.activeSlide = index;
      $scope.images = images;
      options = {
        scope: $scope
      };
      modal_service.from_url('templates/type.html', options).then(function (modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    };
    $scope.closeModal = function () {
      $scope.modal.hide();
      $scope.modal.remove();
    };
    $scope.updateSlideStatus = function (slide) {
      var zoomFactor = $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).getScrollPosition().zoom;
      if (zoomFactor == $scope.zoomMin) {
        $ionicSlideBoxDelegate.enableSlide(true);
      } else {
        $ionicSlideBoxDelegate.enableSlide(false);
      }
    };
    $scope.property = function (issue_id) {
      issue_data = {};
      issue_data.token = token;
      issue_data.issue_id = issue_id;
      issue_data.user_id = user.id;
      ajax_service.post_obj(url + 'property_list_issue', issue_data).then(function (result) {
        console.log(result);
        $scope.property_list = result.data.data;
        $scope.master = result.master;
        if(result.master.video_detail){
          $scope.vid = result.master.video_detail;
          $scope.vid_fn = img_url + $scope.vid.image_path  + $scope.vid.image_name;
        console.log($scope.vid_fn);

        }

      });
    };
    $scope.property(issue_id);
    $scope.videoAlert = function (name, path) {

      console.log(img_url + path + "/" + name);
      $scope.urls = img_url + path + "/" + name;
      myPopup = $ionicPopup.show({
        title: 'Video ', scope: $scope,
        template: '<i class="button ion-close-circled butn_delt_1 "  ng-click="close()"></i> <video class="padding" src="{{urls |trustUrl }}" controls="true" width="230" height="150" > </video> '
      });
    };
    $scope.close = function () {
      myPopup.close();
    };
    $scope.upload = {};
    $scope.issue = {};
    $scope.pathForImage = function (image) {
      if (image === null) {
        return '';
      } else {
        return cordova.file.dataDirectory + image;
      }
    };
    $scope.showPopup = function (issue_id, status) {

      data = {};
      data.token = token;
      data.user_id = user.id;
      ajax_service.post_obj(url + 'getemployeeList', data).then(function (response) {
        $scope.emp_list = response.data;
        angular.forEach($scope.emp_list, function (value,key) {
        // console.log(value.name +'('+ value.user_id + ')' );
        value.emp_name=value.name +'('+ value.user_id + ')'
        });
        // console.log($scope.emp_list);
      });
      $scope.issue_id = issue_id;
      $scope.status = status;
      myPopup = $ionicPopup.show({
        template: ' <label class="item item-input item-select issue_item"><div class="input-label"></div><select class="col-50 txt_2" ng-model="emp_id" required name="Select Option" ng-options="item.id as item.emp_name  for item in emp_list"> <option  disabled selected value="" >Select Option</option></select></label><button class="button button-positive button-small hed_46" ng-click="issue_change(issue_id,status,null,emp_id)">Submit</button><button class="button button-positive button-small hed_46" ng-click="close()">Cancel</button>',
        title: 'Assign To ',
        scope: $scope,
      });
    };
    $scope.maintenance_popup = function (id, status, type) {
      data = {};
      data.token = token;
      data.id = id;
      data.user_id = user.id;
      if(type){
        data.master_type = type;
      }
      ajax_service.post_obj(url + 'work_employees_list', data).then(function (response) {
        $scope.emp_list = response.data;
      });
      $scope.issue_id = issue_id;
      $scope.status = status;
      $scope.master_type = type;
      myPopup = $ionicPopup.show({
        template: '<ion-list><ion-checkbox ng-repeat="item in emp_list" ng-model="item.checked">{{item.name}}</ion-checkbox></ion-list><button class="button button-positive button-small hed_46" ng-click="issue_change(issue_id,status,master_type,emp_list)">Submit</button><button class="button button-positive button-small hed_46" ng-click="close()">cancel</button>',
        title: 'Select Assistant',
        scope: $scope,
      });
    };
    $scope.close = function () {
      myPopup.close();
    };
    $scope.work_popup=function(issue_id, status){
      $scope.issue_id=issue_id;
      $scope.status=status;
      myPopup = $ionicPopup.show({
        template: '<ion-list><ion-radio ng-model="type" value="quote">Get Quote </ion-radio><ion-radio ng-model="type" value="issue">Issue</ion-radio></ion-list><button class="button button-positive button-small hed_46" ng-click="issue_change(issue_id,status,type)">Submit</button><button class="button button-positive button-small hed_46" ng-click="close()">Cancel</button>',
        title: 'Type Of Work',
        scope: $scope,
      });
    };
    $scope.issue_change = function (issue_id, status,type,emp_id) {
      console.log(issue_id, status,type,emp_id);
      issue_data = {};
      issue_data.token = token;
      issue_data.issue_id = issue_id;
      issue_data.issue_status = status;
      issue_data.user_id = user.id;
       if (type) {
         issue_data.type_of_work = type;
       }
      if (user.admin_role_id != 8){
      if(user.admin_role_id == 5|| user.admin_role_id == 6){issue_data.master_type = 'master';}
        if(emp_id){
          myPopup.close();
          issue_data.prop_head_assist_id = emp_id;
        }
        } else {
          issue_data.technician_id = emp_id;
        }
        console.log(issue_data);
      ajax_service.post_obj(url + 'issue_tracking_store', issue_data).then(function (response) {
        console.log(response);
        myPopup1 = $ionicPopup.show({
          title: '<p class="text-center balanced ">Issue Confirmed Successfully</p>'
        });
        $timeout(function () {
          // myPopup.close();
          myPopup1.close();
          $state.go('app.home_page');
        }, 2000);
      });
    };

  })
  .controller('issue_status_ctrl', function ($scope, ajax_service, $timeout,$ionicHistory, $ionicModal, $state, $interval, $ionicPopup, $ionicPopover) {

    $scope.page = 1;
    $scope.limit = 1;
    var myPopup = "";
    $scope.property = function () {
      issue_data = {};
      issue_data.token = token;
      issue_data.user_id = user.id;
      ajax_service.post_obj(url + 'issue_master_list', issue_data).then(function (result) {
        console.log(result);

      if(result.data.length > 0){
        console.log(result);
       $scope.issues = result.data;
      }else{
       $scope.emptyimg=true;
          }
          $timeout( function() {
            $scope.$broadcast('scroll.refreshComplete');
          }, 1000);
      });
    };
    $scope.property();

    $scope.showPopup = function (issue_id, status) {
console.log(issue_id, status);
// alert(issue_id, status);

      if(user.admin_role_id == 5){
        if(status == 'Waiting For Confirmation'){
          data = {};
          data.token = token;
          data.user_id = user.id;
          ajax_service.post_obj(url + 'getemployeeList', data).then(function (response) {
            $scope.emp_list = response.data;
            angular.forEach($scope.emp_list, function (value,key) {
            // console.log(value.name +'('+ value.user_id + ')' );
            value.emp_name=value.name +'('+ value.user_id + ')'
            });
            // console.log($scope.emp_list);
          });
          $scope.issue_id = issue_id;
          $scope.status = status;
          myPopup1 = $ionicPopup.show({
            template: ' <label class="item item-input item-select issue_item"><div class="input-label"></div><select class="col-50 txt_2" ng-model="emp_id" required name="Select Option" ng-options="item.id as item.emp_name  for item in emp_list"> <option  disabled selected value="" >Select Option</option></select></label><button class="button button-positive button-small hed_46" ng-click="issue_change(issue_id,status,null,emp_id)">Submit</button><button class="button button-positive button-small hed_46" ng-click="close()">Cancel</button>',
            title: 'Assign To ',
            scope: $scope,
          });
        }else{
          $scope.issue_change(issue_id,status,null,null)
        }
    }else{
      $scope.issue_change(issue_id,status,null,null)
    }
};
$scope.close = function (){
 myPopup1.close();
}

      $scope.issue_change = function (issue_id, status, reason,emp_id) {
        console.log(issue_id, status, reason)
        // $ionicLoading.show();
        issue_data = {};
        issue_data.token = token;
        issue_data.issue_id = issue_id;
        issue_data.issue_status = status;
        issue_data.user_id = user.id;
        if (reason != null) {
         issue_data.reason = reason;
          myPopup1.close();
        }
        $scope.close();
        issue_data.master_type = "master";
        ajax_service.post_obj(url + 'issue_tracking_store', issue_data).then(function (response) {
          $scope.issue = response.data;
          $scope.property();
          // $timeout(function () {
          //   $scope.close();
          //   $ionicHistory.nextViewOptions({
          //     disableBack: true,
          //     disableAnimate: true,
          //     historyRoot: true
          // });
          //   $ionicHistory.clearCache();
          //   $ionicHistory.clearHistory();
          //   $state.go('app.home_page');
          // }, 2000);
        });
      };


    Issue_status_list = function () {
      issue_data = {};
      issue_data.token = token;
      issue_data.user_id = user.id;
      ajax_service.post_obj(url + 'Issue_status_list', issue_data).then(function (response) {
        $scope.issue_status_list = response.data;
        console.log(response);
      });
    };
    Issue_status_list();
  })
  .controller('StatusCtrl', function ($scope, $ionicPopover, $ionicPopup, $timeout, $ionicModal, $state, $interval, ajax_service) {
    $scope.category = function (id) {
      status_data = {};
      status_data.token = token;
      if (id) {
        status_data.id = id;
      }
      status_data.user_id = user.id;
      ajax_service.post_obj(url + 'issue_master_list', status_data).then(function (result) {
       console.log(result);
        if (result.data.length == 0) {
          $scope.emptyimg=true;
        } else {
          $scope.status = result.data;
          $scope.assign1 = true;
        }
        $timeout( function() {
          $scope.$broadcast('scroll.refreshComplete');
        }, 1000);
      });
    };
    $scope.category();
    $ionicPopover.fromTemplateUrl('templates/issuebtns.html', {
      scope: $scope,
    }).then(function (popover) {
      $scope.popover = popover;
    });
    $scope.doSomething = function ($event) {
      $scope.popover.show($event);
    };
    $scope.gobuttons = function () {
      $state.go('app.create_issue');
      $scope.popover.hide();
    };
  })
  .controller('ProfileCtrl', function ($scope, $stateParams, $ionicPopup, $state, $cordovaToast, ajax_service, $ionicActionSheet, $cordovaCamera) {
    $scope.Profile_head="Profile";
    tenant_details = function () {
      status_data = {};
      status_data.token = token;
      status_data.user_id = user.id;
      ajax_service.post_obj(url + 'tenant_detail', status_data).then(function (result) {
        $scope.profile = result;
      });
    };
    tenant_details();
    user = JSON.parse(localStorage.getItem('rapro'));
    $scope.profile = user;
    $scope.data = {};
    $scope.change_pass = false;
    $scope.edit_pro = true;
    $scope.type = false;
    $scope.password = function () {
      $scope.type = true;
      $scope.change_pass = true;
    $scope.Profile_head="Change Password";

    };
    $scope.edit_prof = function () {
      $scope.type = true;
      $scope.edit_pro = false;
    $scope.Profile_head="Edit Profile";
    };
    $scope.back = function () {
      $scope.error="";
      $scope.change_pass = false;
      $scope.edit_pro = true;
      $scope.type = false;
    };
    $scope.edit = function (profile_data) {
      profile_data.token = token;
      profile_data.id = user.id;
      ajax_service.post_obj(url + 'update_profile', profile_data).then(function (result) {
        if (result.status == 'success') {
          $scope.error = {};
          $ionicPopup.alert({
            template: result.message
          });
          user = result.data;
          localStorage.setItem('rapro', JSON.stringify(result.data));
          $scope.change_pass = false;
          $scope.edit_pro = true;
          $scope.type = false;
        } else if (result.status == 'error') {
          $scope.error = result.errors;
        }
      });
    };
    $scope.change_password = function (password_data) {
      password_data.token = token;
      password_data.user_id = user.id;
      if (password_data.new_password == password_data.confirm_password) {
        ajax_service.post_obj(url + 'change_password', password_data).then(function (result) {
          if (result.status == 'success') {
            $ionicPopup.alert({
              template: result.message
            });
            $scope.data = {};
            $state.go('app.profile');
            $scope.change_pass = false;
            $scope.edit_pro = true;
            $scope.type = false;
          } else {
            $ionicPopup.alert({
              template: result.message
            });
          }
        });
      } else {
        $ionicPopup.alert({
          template: 'Password Missmatched'
        });
      }
    };
  })
  .controller('TechCtrl', function ($scope, $ionicActionSheet, modal_service, $cordovaCamera, $ionicBackdrop, $ionicModal, $ionicSlideBoxDelegate, $ionicScrollDelegate) {
    $scope.zoomMin = 1;
    $scope.showImages = function (index, images) {
      $scope.activeSlide = index;
      $scope.images = images;
      options = {
        scope: $scope
      };
      modal_service.from_url('templates/type.html', options).then(function (modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    };
    $scope.closeModal = function () {
      $scope.modal.hide();
      $scope.modal.remove();
    };
    $scope.updateSlideStatus = function (slide) {
      var zoomFactor = $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).getScrollPosition().zoom;
      if (zoomFactor == $scope.zoomMin) {
        $ionicSlideBoxDelegate.enableSlide(true);
      } else {
        $ionicSlideBoxDelegate.enableSlide(false);
      }
    };
    $scope.showActionsheet = function () {
      $ionicActionSheet.show({
        titleText: 'ActionSheet Example',
        buttons: [{
          text: '<i class="icon ion-camera "></i> Camera'
        },
        {
          text: '<i class="icon ion-android-image"></i> Gallery'
        },
        ],
        cancelText: 'Cancel',
        cancel: function () { },
        buttonClicked: function (index) {
          if (index == 0) {
            $scope.getPicture('camera');
          }
          if (index == 1) {
            $scope.getPicture('file');
          }
          return true;
        },
      });
    };
    $scope.getPicture = function (type) {
      camera_actions.selImages(type).then(function (imageUri) {
        if (imageUri != undefined) {
          $scope.image_a.push(imageUri);
        }
      });
    };
    $scope.remove = function (item) {
      var index = $scope.image_a.indexOf(item);
      $scope.image_a.splice(index, 1);
    };
    $scope.video_a = '';
    $scope.captureVideo = function () {
      $cordovaCapture.captureVideo(video_option).then(function (videoData) {
        $scope.video_a = videoData[0].fullPath;
      }, function (err) { });
    };
    $scope.issues = [{
      "list": " Tenant not available",
    },
    {
      "list": " Technical issue"
    },
    {
      "list": " Late Arrival"
    },
    {
      "list": "Power cut"
    }
    ];
    $scope.issueslist = function (tech) {
      if ($scope.islistShown(tech)) {
        $scope.shownlist = null;
      } else {
        $scope.shownlist = tech;
      }
    };
    $scope.islistShown = function (tech) {
      return $scope.shownlist === tech;
    };
    $scope.assignname = function (main) {
      if ($scope.isnameShown(main)) {
        $scope.shownlist = null;
      } else {
        $scope.shownlist = main;
      }
    };
    $scope.isnameShown = function (main) {
      return $scope.shownlist === main;
    };
  })
  .controller('payment_history', function ($scope, $ionicPopup, $timeout, ajax_service, $ionicLoading, $filter) {
    var myDate = new Date();
     $scope.data={};
    $scope.pay_tenant = function (no, date) {
      data = {};
      data.token = token;
      data.user_id = user.id;
      data.no = no;
      if (date) {
        data.date = date;
      }
      ajax_service.post_obj(url + 'payment_history', data).then(function (result) {
        if (result.status == 'success') {
          if(result.data.length > 0){
           $scope.emptyimg=false;
            console.log(result);
            $scope.payment_type = result.data;
          }else{
           $scope.emptyimg=true;
           }
           $timeout( function() {
            $scope.$broadcast('scroll.refreshComplete');
          }, 1000);
           }
      });
    };

    $scope.pay = function (data) {

      data.token = token;
      data.user_id = user.id;
      ajax_service.post_obj(url + 'payment_history', data).then(function (result) {
        $scope.detail=true;
        if (result.status == 'success') {
          if(result.data.length > 0){
           $scope.emptyimg=false;
            console.log(result);
            $scope.payment_type = result.data;

          }else{
           $scope.emptyimg=true;
           }
           $timeout( function() {
            $scope.$broadcast('scroll.refreshComplete');
          }, 1000);
          }
      });
    };
    $scope.property = function (data) {

      issue_data = {};
      issue_data.token = token;
      issue_data.user_id = user.id;
      ajax_service.post_obj(url + 'property_name', issue_data).then(function (result) {
        console.log(result);
        $scope.property_list = result;
      });
    };
    if(user.admin_role_id == 4){
      $scope.property();
    }
  })
  .controller('PaymentCtrl', function (camera_actions,modal_service, $scope, $ionicPopup, $timeout, $ionicModal, $cordovaCamera, ajax_service, $cordovaFile, $cordovaFileTransfer, $cordovaDevice, $ionicActionSheet, Upload, $ionicLoading) {
    $scope.createContact = function (u) {
      $scope.contacts.push({
        name: u.firstName + ' ' + u.lastName
      });
    };
    $scope.currentDate = new Date();
    $scope.title = "Custom Title";
    category = function () {
      data = {};
      data.token = token;
      data.user_id = user.id;
      ajax_service.post_obj(url + 'tenant_pay_category', data).then(function (result) {
        $scope.category = result.data;
        console.log(result.data);
      });
    };
    category();

    paid_by_category = function () {
      data = {};
      data.token = token;
      data.user_id = user.id;
      ajax_service.post_obj(url + 'paid_by_category', data).then(function (result) {
        console.log(result);

        if(user.admin_role_id == 5){
        data=result.data;
        $scope.category_paid=data;
        console.log(data);

      }else{
        $scope.category_paid = result.data;
        console.log(result.data);
      }
      });
    };
    paid_by_category();


    $scope.property = function (data) {
      issue_data = {};
      issue_data.token = token;
      issue_data.user_id = user.id;
      ajax_service.post_obj(url + 'property_name', issue_data).then(function (result) {
        console.log(result);
        $scope.property_list = result;
      });
    };
    $scope.property();

    $scope.property_show = function () {
      data = {};
      data.token = token;
      data.user_id = user.id;
      ajax_service.post_obj(url + 'property_name', data).then(function (result) {
        console.log(result);
          $scope.property_name_list = result;
      });
      options = {
        scope: $scope
      };
      modal_service.from_url('templates/property_show.html', options).then(function (res) {
        $scope.modal = res;
        console.log(res);
        $scope.modal.show();
      });
    };

    $scope.property_detail_single=function(data){

      console.log(data);
      $scope.bind=data;
      $scope.closeModal();
    }

    $scope.dcollection = function () {

      ajax_service.post_obj(url + 'get_monthly').then(function (response) {
          $scope.month = response.data;
      });


    };
    $scope.dcollection();
    $scope.reset = function () {
      $scope.amt = {};
      $scope.err = "";
    };
    $scope.amt = {};

    $scope.pathForImage = function (image) {
      if (image === null) {
        return '';
      } else {
        return cordova.file.dataDirectory + image;
      }
    };
    $scope.payment_detail = function (amt, photos,id) {

      amt.property_id=id;
      if(amt.category != "Rent" && amt.category != "EB"){
       amt.month_year = null;
    }
      if (photos) {
        img_params = {
          path: 'payment',
          type: 'photos',
        };
        camera_actions.file_upload(url + 'upload_file', photos, img_params).then(function (res) {
          if(res.status == "success"){
            amt.token = token;
            amt.user_id = user.id;
            amt.imgs = res.response;
            ajax_service.post_obj(url + 'payment_detail', amt).then(function (response) {
              pay_response(response);
            });
          }else{

            myPopup = $ionicPopup.show({
              title: '<p class="positive">Network Error !! Try after sometime!!</p>'
            });
            $timeout(function () {
              myPopup.close();
            }, 3000);
          }
        });
      } else {
        amt.token = token;
        amt.user_id = user.id;

        ajax_service.post_obj(url + 'payment_detail', amt).then(function (response) {
          pay_response(response);
        });
         }
    };
    $scope.remove = function () {
      $scope.image_a = '';
    };
    var pay_response = function (response) {
         if (response.status == 'success') {
        $scope.image_a = '';
        $scope.bind={};
        $scope.tnt_err="";
        $scope.message = response.message;
        $scope.amt = {};
        $scope.err = {};
        $scope.validation = {};
        $scope.err_word = {};
        myPopup = $ionicPopup.show({
          title: '<p class="balanced">Receipt Submitted Successfully </p>'
        });
        $timeout(function () {
          myPopup.close();
        }, 3000);
      } else {
        if(response.meg){
          $scope.tnt_err=response.meg;
        }
        $scope.message = response.message;
        $scope.validation = {};
        $scope.err = {};
        $scope.err_word = {};
        angular.forEach(response.errors, function (value, key) {
          $scope.validation[key] = value[1];
          $scope.err[key] = 'red_border';
          $scope.err_word[key] = value[0];
        });


        // alert(JSON.stringify(response.msg));
        // myPopup = $ionicPopup.show({
        //   title: '<p class="assertive">Please fill all mandatory fields</p>'
        // });
        $timeout(function () {
          $scope.msgs = "";
          myPopup.close();
        }, 3000);
      }
    };
    $scope.showActionsheet = function () {
      $ionicActionSheet.show({
        titleText: 'ActionSheet Example',
        buttons: [{
          text: '<i class="icon ion-camera "></i> Camera'
        },
        {
          text: '<i class="icon ion-android-image"></i> Gallery'
        },
        ],
        cancelText: 'Cancel',
        cancel: function () {
          console.log('CANCELLED');
        },
        buttonClicked: function (index) {
          if (index == 0) {
            $scope.getPicture('camera');
          }
          if (index == 1) {
            $scope.getPicture('file');
          }
          return true;
        },
      });
    };
    $scope.getPicture = function (type) {
      camera_actions.selImages(type).then(function (imageUri) {
        if (imageUri != undefined) {
          $scope.image_a = imageUri;
        }
      });
    };

    $scope.showImages = function (index, images) {
      $scope.activeSlide = index;
      $scope.image = images;
      options = {
        scope: $scope
      };
      modal_service.from_url('templates/type2.html', options).then(function (modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    };
    $scope.closeModal = function () {
      $scope.modal.hide();
      $scope.modal.remove();
    };
  })
  .controller('customer_PropertyCtrl', function ($cordovaToast, modal_service, $scope, $ionicBackdrop, ajax_service, $state,$cordovaFileOpener2, $stateParams, $ionicModal, $ionicSlideBoxDelegate, $cordovaFile, $cordovaFileTransfer, $cordovaDevice,$ionicPopup) {
    var id = $stateParams.id;
    $scope.single_prop = {};
    $scope.img_url = img_url;

    property_detail_list = function (id) {
      data = {};
      data.token = token;
      data.user_id = user.id;
      data.property_id = id;
      ajax_service.post_obj(url + 'customer_property_list', data).then(function (result) {
        console.log(result);
        if (result.status == 'success') {
          $scope.single_prop = result.data;
          $scope.img_url = img_url;
        }
      });
    };
    property_detail_list(id);
    $scope.zoomMin = 1;
    $scope.showImages = function (index, images) {
      $scope.activeSlide = index;
      $scope.images = images;
      options = {
        scope: $scope
      };
      modal_service.from_url('templates/type.html', options).then(function (modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    };

    $scope.closeModal = function () {
      $scope.modal.hide();
      $scope.modal.remove();
    };
    $scope.tab = 1;
    $scope.setTab = function (newTab) {
      $scope.tab = newTab;
    };
    $scope.isSet = function (tabNum) {
      return $scope.tab === tabNum;
    };
    $scope.toggleGroup = function (group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };
    $scope.isGroupShown = function (group) {
      return $scope.shownGroup === group;
    };
    $scope.file_download = function (path, name, status) {

      var url = img_url + path + "/" + name;
      var aggname = (status == 1) ? "Maintanence Agreement" : "PM Agreement";
      if (ionic.Platform.isIOS()) var targetPath = cordova.file.dataDirectory + "/Download/"+ aggname + name;
else if(ionic.Platform.isAndroid()) var targetPath = cordova.file.externalRootDirectory + "/Download/"+ aggname + name;;
      var trustHosts = true;
      var options = {};
      var httpurl = 'https://docs.google.com/viewer?url=' + url;
      window.open(httpurl, '_system', 'location=yes');
      $cordovaFileTransfer.download(url, targetPath, options, trustHosts)
        .then(function (result) {
          $cordovaToast.showShortBottom("file downloaded" +targetPath);
          result.file(function (file) {
       var localFile = file.localURL;
       resolveLocalFileSystemURL(localFile, function (entry) {
           var nativePath = entry.toURL();
           var alertPopup = $ionicPopup.alert({
             title: 'Download Location',
             template: JSON.stringify(nativePath)
           });
       }, function(error) {
           //handle error here
       });
   }, function (error) {
       // handle error here
   });
        }, function (err) {
          // alert(JSON.stringify(err));
        });
    };
  })
  .controller('PropertyCtrl', function ($stateParams,$scope,$timeout,modal_service, ajax_service, $ionicModal, $state) {
    const update_id = $stateParams.id;
    // alert(JSON.stringify(update_id));
    $scope.img_url = img_url;
    $scope.page = 1;
    $scope.limit = 1;
    $scope.property_list = function () {
      data = {};
      data.token = token;
      if(update_id){
        data.user_id = update_id;
      }else{
        data.user_id = user.id;
      }
      data.type = 'api';
      ajax_service.post_obj(url + 'customer_property_list', data).then(function (result) {
        console.log(result.data.data);
        if(result.data.data.length >0){
        $scope.property = result.data.data;
        console.log(result.data);
       }else{
        $scope.emptyimg=true;
       }

        $timeout( function() {
          $scope.$broadcast('scroll.refreshComplete');
        }, 1000);
      });
    };
    $scope.property_list();
    $scope.showImages = function (index, images) {
      $scope.activeSlide = index;
      $scope.images = images;
      options = {
        scope: $scope
      };
      modal_service.from_url('templates/type.html', options).then(function (modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    };
    $scope.closeModal = function () {
      $scope.modal.hide();
      $scope.modal.remove();
    };
  })
  // .controller('PropertyDetailCtrl', function ($scope, $ionicModal, $stateParams, Property) {
  // })
  // .controller('HeadCtrl', function ($scope) {

  // })
  .controller('ServiceCtrl', function ($scope, modal_service, $rootScope, $ionicModal, $ionicScrollDelegate, ajax_service, $location, $timeout) {
    $scope.img_url = img_url;
    options = {
      scope: $scope
    };
    modal_service.from_url('templates/service_model.html', options).then(function (modal) {
      $scope.service = modal;
    });
    $scope.serviceOpen = function (slide, image, title) {
      $scope.slide = slide;
      $scope.image = image;
      $scope.title = title;
      $scope.service.show();
    };
    $scope.serviceClose = function () {
      $scope.service.hide();
    };
    $scope.groups = [];
    for (var i = 0; i < 10; i++) {
      $scope.groups[i] = {
        name: i,
        items: []
      };
      for (var j = 0; j < 3; j++) {
        $scope.groups[i].items.push(i + '-' + j);
      }
    }
    $scope.slideTo = function (location) {
      location = $location.hash(location);
      $timeout(function () {
        $ionicScrollDelegate.anchorScroll("#" + location);
      });
    };
    $scope.toggleGroup = function (group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };
    $scope.isGroupShown = function (group) {
      return $scope.shownGroup === group;
    };
    $scope.service_detail = function () {
      data = {};
      data.token = token;
      ajax_service.post_obj(url + 'service_detail', data).then(function (result) {
        $scope.image = result;
      });
    };
    $scope.service_detail();
  })
  .controller('property_head_ctrl', function ($scope, ajax_service, $ionicPopup, $timeout, $ionicModal, $state, $interval,$ionicScrollDelegate) {
    $scope.progressval = 0;
    $scope.stopinterval = null;
    $scope.stop_loadmore = true;

    $scope.updateProgressbar = function () {
      startprogress();
    };
    function startprogress() {

      $scope.progressval = 0;
      if ($scope.stopinterval) {
        $interval.cancel($scope.stopinterval);
      }
      $scope.stopinterval = $interval(function () {
        $scope.progressval = $scope.progressval + 1;
        if ($scope.progressval >= 60) {
          $interval.cancel($scope.stopinterval);
          return;
        }
      }, 60);
    }
    page = 0;
    $scope.current_page_no = 0;
  $scope.current_page_no_2 = 0;
    $scope.cardss=function(no){
      console.log(no);
      $scope.field="";
     $scope.card=(no == 1) ?  "vacant" : "occupied";
     $scope.current_page_no = 1;
     $scope.current_page_no_2 = 1;
     $scope.customer_load(1,$scope.card);
      $ionicScrollDelegate.scrollTop();
    }

    $scope.refresh=function(categories){
      console.log(categories);
      $scope.properties={};
    $scope.customer_load(1,categories);
    $scope.current_page_no = 0;
    $scope.current_page_no_2 = 0;

    }
    $scope.domore=function(group_by){
      console.log(group_by);

      $scope.card=(group_by) ? group_by : "vacant";

      if(group_by  == "occupied"){
        $scope.current_page_no += 1;
        $scope.customer_load($scope.current_page_no,group_by);
      }else{
        $scope.current_page_no_2 += 1;
        $scope.customer_load($scope.current_page_no_2,'vacant');

      }

    }
    $scope.customer_load = function (page_no,group_by) {
      data = {};
      data.token = token;
      data.user_id = user.id;
      data.type = "api";
      data.items_per_pages=5;
      data.current_page_no=page_no;
      data.property_status=(group_by == 'vacant') ? 'available' : 'notavailable';
      console.log(data);
      ajax_service.post_obj(url + 'property_list', data).then(function (result) {
        console.log(result,page_no);

        if (result.status == 'success') {
          if(result.data.length > 0){
           $scope.emptyimg=false;
           $scope.stop_loadmore=true;
            if(group_by == "vacant"){
              console.log('vacant',page_no);
               $scope.show_repeat=true;
               $scope.show_repeat_a=false;
               if (page_no  == 1) {
                 $scope.properties=result.data;
                 } else {
                  $scope.properties = $scope.properties.concat(result.data);
                  }
             }else{
             console.log('occupied');
             $scope.show_repeat=false;
             $scope.show_repeat_a=true;

             if (page_no  == 1) {
                 $scope.properties_oc=result.data;
                 } else {

                  $scope.properties_oc = $scope.properties_oc.concat(result.data);

                  }
             }
              $scope.occ = result.occupied;
               $scope.vac = result.vacant;
               $scope.$broadcast('scroll.refreshComplete');
              $scope.$broadcast('scroll.infiniteScrollComplete');
          }else if(page_no  == 1){
           $scope.emptyimg=true;
          }else{
           $scope.stop_loadmore=false;

          }
        }
         });
    };

    $scope.search_field=function(value,property_status){
      console.log(value,property_status);
      $ionicScrollDelegate.scrollTop();
      data={}
      data.type = "api";
      data.search=value;
      data.token=token;
      data.user_id = user.id;
      data.property_status=(property_status == 'vacant') ? 'available' : 'notavailable';
      if(!value){
      data.items_per_pages=5;
      data.current_page_no=1;
      }
          ajax_service.post_obj(url + 'property_list', data).then(function (result) {
            console.log
            $scope.stop_loadmore = false;
            if(result.data.length > 0){
            if(status == "vacant"){
               $scope.show_repeat=true;
               $scope.show_repeat_a=false;
               $scope.properties=result.data;

             }else{
             console.log('occupied');
             $scope.show_repeat=false;
             $scope.show_repeat_a=true;
             $scope.properties_oc=result.data;
             }
            }else{
              if(status == "vacant"){
                $scope.properties ="";
              }else{
                $scope.properties_oc ="";
              }
              $scope.show_empty=true;
            }
           });
    }
    // $scope.domore("vacant");
    startprogress();
  })
  .controller('property_details_ctrl', function ($scope, modal_service, $ionicModal, ajax_service, $stateParams) {
    property_id = $stateParams.property_id;
    data = {};
    data.token = token;
    data.property_id = property_id;
    console.log(property_id);
    ajax_service.post_obj(url + 'property_details', data).then(function (result) {
      if (result.status == 'success') {
        console.log(result);
        $scope.property_details = result.data;
      }
    });
    $scope.showImages = function (index, images) {
      $scope.activeSlide = index;
      $scope.images = images;
      options = {
        scope: $scope
      };
      modal_service.from_url('templates/type.html', options).then(function (modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    };
    $scope.closeModal = function () {
      $scope.modal.hide();
      $scope.modal.remove();
    };
  })
  .controller('DateCtrl', function ($scope,$ionicHistory,$ionicViewService, $interval) {
    // $ionicHistory.clearHistory();
    //  $ionicViewService.clearHistory()
    $scope.today = new Date();
    $interval(function () {
      $scope.today = new Date();
    }, 1000);
  })
  .controller('NewenquryCtrl', function ($scope, camera_actions, $ionicPopup, $ionicActionSheet, ajax_service, $cordovaCamera, $cordovaProgress, $cordovaCapture, $ionicLoading, $timeout, $state) {
    category_list = function () {
      category_data = {};
      category_data.token = token;
      ajax_service.post_obj(url + 'issue_categories', category_data).then(function (result) {
        // alert(JSON.stringify(result));
        if (result.status == "success") {
          $scope.categories = result.data;
        } else {

        }
      });
    };
    category_list();
    $scope.showActionsheet = function () {
      $ionicActionSheet.show({
        titleText: 'ActionSheet Example',
        buttons: [{
          text: '<i class="icon ion-camera "></i> Camera'
        },
        {
          text: '<i class="icon ion-android-image"></i> Gallery'
        },
        ],
        cancelText: 'Cancel',
        cancel: function () { },
        buttonClicked: function (index) {
          if (index == 0) {
            $scope.getPicture('camera');
          }
          if (index == 1) {
            $scope.getPicture('file');
          }
          return true;
        },
      });
    };
    $scope.image_a = [];
    $scope.getPicture = function (type) {
      camera_actions.selImages(type).then(function (imageUri) {
        if (imageUri != undefined) {
          $scope.image_a.push(imageUri);
        }
      });
    };
    $scope.remove = function (item) {
      var index = $scope.image_a.indexOf(item);
      $scope.image_a.splice(index, 1);
    };
    $scope.showImages = function (index, images) {
      $scope.activeSlide = index;
      $scope.images = images;
      options = {
        scope: $scope
      };
      modal_service.from_url('templates/type.html', options).then(function (modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    };
    $scope.closeModal = function () {
      $scope.modal.hide();
      $scope.modal.remove();
    };
    $scope.enquiry_input = {};
    $scope.photos = {};
    $scope.fieldcheck = function (enquiry_input, photos) {

      enquiry_input.token = token;
      // $ionicLoading.show({
      //   template: '<ion-spinner icon="ios" class="spinner"></ion-spinner></br>Loading'
      // });
      if (photos.length == 0) {
        ajax_service.post_obj(url + 'new_customer_enquiry', enquiry_input).then(function (response) {
          enq_response(response);
        });
      } else {
        file_ids = [];
        img_params = {
          path: 'enquiry',
          type: 'photos',
        };
        angular.forEach(photos, function (value) {
          camera_actions.file_upload(url + 'upload_file', value, img_params).then(function (res) {
            file_ids.push(res.response);
            if (photos.length == file_ids.length) {
              enquiry_input.token = token;
              enquiry_input.user_id = user.id;
              enquiry_input.image = file_ids;

              ajax_service.post_obj(url + 'new_customer_enquiry', enquiry_input).then(function (response) {
                enq_response(response);

              });
            }
          });
        });
      }
    };
    var enq_response = function (response) {
      // $ionicLoading.hide();
      if (response.status == 'success') {
        $scope.message = response.message;
        $scope.enquiry_input = {};
        // $ionicLoading.hide();
        $scope.err = {};
        $scope.image_a = {};
        $scope.validation = {};
        $scope.err_word = {};
        myPopup = $ionicPopup.show({
          title: '<p class="balanced">Your Enquiry Has Been Sent</p>'
        });
        $timeout(function () {
          $scope.msg = "";
          myPopup.close();
          $state.go('first_page');
        }, 3000);
      } else if (response.status == 'error') {
        $scope.message = response.message;
        $scope.validation = {};
        $scope.err = {};
        $scope.err_word = {};
        myPopup = $ionicPopup.show({
          title: '<p class="text-center assertive">Fill all the fields</p>'
        });
        angular.forEach(response.errors, function (value, key) {
          $scope.validation[key] = value[1];
          $scope.err[key] = 'red_border';
          $scope.err_word[key] = value[0];
        });
        $timeout(function () {
          $scope.msgs = "";
          myPopup.close();
        }, 1000);
      }
    };
    $scope.exist_usr_detail = function (user_check) {
      data = {};
      data.token = token;
      data.user_check = user_check;
      ajax_service.post_obj(url + 'existing_customer', data).then(function (response) {
        if (response.status == 'error') {
          $scope.err = {};
          $scope.validation = {};
          $scope.err_word = {};
          angular.forEach(response.errors, function (value, key) {
            $scope.validation[key] = value[1];
            $scope.err[key] = 'red_border';
            $scope.err_word[key] = value[0];
          });
        } else if (response.status == 'success') {
          $scope.err = {};
          $scope.validation = {};
          $scope.err_word = {};
          if (response.data.length != 0) {
            $scope.ids = response.data;
            $state.go('exist_detail', {
              ids: $scope.ids
            });
          } else if (response.data.length == 0) {
            $ionicPopup.alert({
              template: '<p class="text-center assertive">User Id missmatched</p>'
            });
          }
        }
      });
    };
    $scope.exist = function (y) {
      if ($scope.isexistShown(y)) {
        $scope.Shown = null;
      } else {
        $scope.Shown = y;
      }
    };
    $scope.isexistShown = function (y) {
      return $scope.Shown === y;
    };
    $scope.enquiry = function (x) {
      if ($scope.isenquiryShown(x)) {
        $scope.shown = null;
      } else {
        $scope.shown = x;
      }
    };
    $scope.isenquiryShown = function (x) {
      return $scope.shown === x;
    };
    $scope.submit = function (z) {
      if ($scope.issubmitShown(z)) {
        $scope.shown = null;
      } else {
        $scope.shown = z;
      }
    };
    $scope.issubmitShown = function (z) {
      return $scope.shown === z;
    };
    $scope.shoe = function (num) {
      if (num == "x") {
        $scope.xshow = true;
      } else if (num == "y") {
        $scope.yshow = true;
      }
    };
  })
  .controller('exist_enquryCtrl', function (camera_actions, $scope, $ionicModal, $stateParams, $ionicPopup, $ionicActionSheet, ajax_service, $cordovaCamera, $cordovaToast, $cordovaCapture, $ionicLoading, $timeout, $state,modal_service) {
    // var users_id = $stateParams.ids;
    category_list = function () {
      category_data = {};
      category_data.token = token;
      ajax_service.post_obj(url + 'issue_categories', category_data).then(function (result) {
        if (result.status == "success") {
          $scope.categories = result.data;
        } else { }
      });
    };
    category_list();
    $scope.showActionsheet = function () {

      $ionicActionSheet.show({
        titleText: 'ActionSheet Example',
        buttons: [{
          text: '<i class="icon ion-camera "></i> Camera'
        },
        {
          text: '<i class="icon ion-android-image"></i> Gallery'
        },
        ],
        cancelText: 'Cancel',
        cancel: function () { },
        buttonClicked: function (index) {
          if (index == 0) {
            $scope.getPicture('camera');
          }
          if (index == 1) {
            $scope.getPicture('file');
          }
          return true;
        },
      });
    };
    $scope.image_a = [];
     // alert(JSON.stringify(image_a));
    $scope.getPicture = function (type) {

      camera_actions.selImages(type).then(function (imageUri) {
        if (imageUri != undefined) {

          $scope.image_a.push(imageUri);
        }
      });
    };
    $scope.enquiry_input = {};
    $scope.photos = {};
    $scope.existing_customer_check = function (enquiry_input, photos, user_id) {
      console.log(enquiry_input);
      enquiry_input.token = token;
      enquiry_input.users_id = user.id;
      // $ionicLoading.show({
      //   template: '<ion-spinner icon="ios" class="spinner"></ion-spinner></br>Loading'
      // });
      if (photos == 0) {
        ajax_service.post_obj(url + 'new_customer_enquiry', enquiry_input).then(function (response) {
          enr_res(response);
        });
      } else {
        file_ids = [];
        img_params = {
          path: 'enquiry',
          type: 'photos',
        };
        angular.forEach(photos, function (value) {
          camera_actions.file_upload(url + 'upload_file', value, img_params).then(function (res) {
            if (res.status == "success") {
              file_ids.push(res.response);
              if (photos.length == file_ids.length) {
                enquiry_input.token = token;
                enquiry_input.user_id = user.id;
                enquiry_input.image = file_ids;
                ajax_service.post_obj(url + 'new_customer_enquiry', enquiry_input).then(function (response) {
                  enr_res(response);
                });
              }
            } else {
              $cordovaToast.showShortBottom("Something went wrong");
            }
          });
        });
      }
    };
    $scope.remove = function (item) {
      var index = $scope.image_a.indexOf(item);
      $scope.image_a.splice(index, 1);
    };
    var enr_res = function (response) {
      // $ionicLoading.hide();
      console.log(response);

      if (response.status == 'success') {
        $scope.message = response.message;
        $scope.image_a = {};
        $scope.enquiry_input = {};
        myPopup = $ionicPopup.show({
          title: '<p class="text-center balanced">Your Enquiry Has Been Sent</p>'
        });
        $scope.err = {};
        $scope.validation = {};
        $scope.err_word = {};
        $timeout(function () {
          myPopup.close();
          console.log(response.user);

          if(response.user == "exist"){
            $state.go('app.home_page');
          }else{
            $state.go('first_page');
          }
        }, 3000);
      } else if (response.status == 'error') {
        $scope.message = response.message;
        $scope.validation = {};
        $scope.err = {};
        // $ionicLoading.hide();
        $scope.err_word = {};
        myPopup = $ionicPopup.show({
          title: '<p class="text-center assertive">Fill All Mandatory Fields</p>'
        });
        angular.forEach(response.errors, function (value, key) {
          $scope.validation[key] = value[1];
          $scope.err[key] = 'red_border';
          $scope.err_word[key] = value[0];
        });
        $timeout(function () {
          $scope.msgs = "";
          myPopup.close();
        }, 1000);
      }
    };
    $scope.showImages = function (index, images) {
      $scope.activeSlide = index;
      $scope.images = images;
      options = {
        scope: $scope
      };
      modal_service.from_url('templates/type.html', options).then(function (modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    };
    $scope.closeModal = function () {
      $scope.modal.hide();
      $scope.modal.remove();
    };
  })
  // .controller('EnquiryCtrl', function ($scope, $ionicModal) {
  // })
  .controller('ExpenseCtrl', function ($scope, MonthPicker, ajax_service) {
    $scope.img_url = img_url;
    $scope.page = 1;
    $scope.limit = 1;
    MonthPicker.init({});
    $scope.buttonTap = function () {
      MonthPicker.show(function (res) { });
    };
    $scope.property_list = function () {
      data = {};
      data.token = token;
      data.user_id = user.id;

      ajax_service.post_obj(url + 'customer_property_list', data).then(function (result) {
        $scope.property = result.data;
        console.log(result);

      });
    };
    $scope.property_list();
  })

  .controller('MainCtrl', function ($scope, $state, $interval) {
    $scope.progressval = 0;
    $scope.stopinterval = null;
    $scope.updateProgressbar = function () {
      startprogress();
    };

    function startprogress() {
      $scope.progressval = 0;
      if ($scope.stopinterval) {
        $interval.cancel($scope.stopinterval);
      }
      $scope.stopinterval = $interval(function () {
        $scope.progressval = $scope.progressval + 1;
        if ($scope.progressval >= 60) {
          $interval.cancel($scope.stopinterval);
          return;
        }
      }, 60);
    }
    startprogress();
  })
  .controller('leave_management_ctrl', function ($scope, $state, ajax_service, $ionicLoading, $ionicPopup, $timeout) {
    $scope.lev = {};
    var myPopup = "";
    $scope.date = new Date().toDateString();
    $scope.leave_list_all=function(){
      data.token = token;
      data.user_id = user.id;
      ajax_service.post_obj(url + 'getEmployeeLeave', data).then(function (response) {
        console.log(response);
        if(response.data.length > 0){
          $scope.leave_list=response.data;
           }
        $timeout( function() {
          $scope.$broadcast('scroll.refreshComplete');
        }, 500);
      });
    }
    $scope.leave_req = function (lev) {
      // $ionicLoading.show();
      // if (lev.enddate - lev.startdate != 0) {
        lev.token = token;
        lev.user_id = user.id;
        ajax_service.post_obj(url + 'leave_request', lev).then(function (response) {
          if (response.status == 'success') {
            // $ionicLoading.hide();
            $scope.leave_list_all();
            myPopup = $ionicPopup.show({
              title: response.message
            });
            $scope.message = response.message;
            $scope.samedate = "";
            $scope.lev = {};
            $scope.msg = 'balanced';
            $scope.err = {};
            $scope.validation = {};
            $scope.err_word = {};
            $timeout(function () {
              $scope.msg = "";
              myPopup.close();
              // if (response.message == "leave Request Send") { $state.go('app.home_page'); }
            }, 3000);
          } else if (response.status == 'error') {
            // $ionicLoading.hide();
            myPopup = $ionicPopup.show({
              title: '<p class="text-center assertive">Error!!!!!</p>'
            });
            $scope.message = response.message;
            $scope.msgs = 'assertive';
            $scope.validation = {};
            $scope.err = {};
            $scope.err_word = {};
            angular.forEach(response.errors, function (value, key) {
              $scope.validation[key] = value[1];
              $scope.err[key] = 'red_border';
              $scope.err_word[key] = value[0];
            });
            $timeout(function () {
              myPopup.close();
              $scope.msgs = "";
              $scope.err = "";
            }, 3000);
          }
        });
      // } else {
      //   $ionicLoading.hide();
      //   $scope.samedate = 'please choose valid date';
      // }
    };
    $scope.reset = function () {
      $scope.lev = {};
      $scope.err_word = "";
    };
    $scope.moreDataCanBeLoaded = true;
    page = 0;
    $scope.list = [];
    $scope.leavelistforall = function (page_no) {
       data = {};
      data.token = token;
      data.user_id = user.id;
      ajax_service.post_obj(url + 'pha_leave_request_list', data).then(function (response) {
        console.log(response);
        if (response.data.length > 0) {
          $scope.lev_listed = response.data;
          $scope.$broadcast('scroll.refreshComplete');
        } else {
          $scope.emptyimg = true;
        }
        $scope.leave_list_all();
      });
    };

    $scope.showPopup = function (leave_request_id, status) {

      $scope.leave_request_id = leave_request_id;
      $scope.status = status;
      myPopup = $ionicPopup.show({
        template: '<textarea rows="4" cols="30" placeholder=" Enter Description" ng-model="comment" ></textarea><button class="button button-positive button-small hed_46 " ng-click="leave_status(leave_request_id, status,comment,1)">Submit</button><button class="button button-positive button-small hed_46" ng-click="close()">Cancel</button>',
        title: 'Reason For Decline',
        scope: $scope,
      });
    };

    $scope.close = function () {
      myPopup.close();
    };
    $scope.leavelistforall();
    $scope.leave_status = function (leave_request_id, status, comment, id) {
      if (id == 1) { myPopup.close(); }
      // $ionicLoading.show();
      data = {};
      data.status = status;
      data.comment = comment;
      data.leave_request_id = leave_request_id;
      data.token = token;
      data.user_id = user.id;
      ajax_service.post_obj(url + 'leave_request_status', data).then(function (response) {

        // $ionicLoading.hide();
        if (response.status == "success") {
          myPopup = $ionicPopup.show({
            title: response.message
          });
          $timeout(function () {
            myPopup.close();
            $scope.leavelistforall();
            // $state.go('app.home_page');
          }, 2000);
        }
      });
    };
  })
  .controller('IssueDetailCtrl', function ($scope, $ionicBackdrop, ajax_service, modal_service, $ionicSlideBoxDelegate, $ionicScrollDelegate, $stateParams, $ionicLoading, $state, $ionicPopup, $timeout) {
    var myPopup = "";
    var issue_id = $stateParams.issue_id;
    $scope.issue_details = {};
    issue_details_view = function (issue_id) {
      console.log(issue_id);
      data = {};
      data.token = token;
      data.user_id = user.id;
      data.issue_id = issue_id;
      ajax_service.post_obj(url + 'issue_detail', data).then(function (result) {
        if (result.status == 'success') {
          $scope.issue_details = result.data;
          $scope.img_url = img_url;
          if (result.data.video) {
            $scope.vid = result.data.video;
            $scope.vid_fn = img_url + $scope.vid.image_path + '/' + $scope.vid.image_name;
          }
        }
      });
    };
    issue_details_view(issue_id);
    $scope.zoomMin = 1;
    $scope.showImages = function (index, images) {
      $scope.activeSlide = index;
      $scope.images = images;
      options = {
        scope: $scope
      };
      modal_service.from_url('templates/type.html', options).then(function (modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    };
    $scope.closeModal = function () {
      $scope.modal.hide();
      $scope.modal.remove();
    };
    $scope.updateSlideStatus = function (slide) {
      var zoomFactor = $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).getScrollPosition().zoom;
      if (zoomFactor == $scope.zoomMin) {
        $ionicSlideBoxDelegate.enableSlide(true);
      } else {
        $ionicSlideBoxDelegate.enableSlide(false);
      }
    };
    $scope.detail_modal = '';
    $scope.show_detail = function () {
      options = {
        scope: $scope,
        animation: 'slide-in-up'
      };
      modal_service.from_url('templates/property_head/issue_status_detail.html', options).then(function (modal) {
        $scope.detail_modal = detail_modal;
        $scope.detail_modal.show();
      });
    };
    $scope.detail_modal_close = function () {
      $scope.detail_modal.hide();
    };
    $scope.update_issue_status = function (issue_id) {
      // $ionicLoading.show({
      //   template: '<ion-spinner icon="ios" class="spinner"></ion-spinner>'
      // });
      issue_update_data = {};
      issue_update_data.token = token;
      issue_update_data.issue_id = issue_id;
      issue_update_data.status = 'Issue Confirmed';
      ajax_service.post_obj(url + 'issue_update', issue_update_data).then(function (result) {
        if (result.status == 'success') {
          $scope.maintenance_list = result.data;
        }
      });
    };
    $scope.issue_to_maintanance = function (issue_id) {
      // $ionicLoading.show();
      issue_update_data = {};
      issue_update_data.token = token;
      issue_update_data.user_id = user.id;
      issue_update_data.issue_id = issue_id;
      issue_update_data.issue_status = 'Issue Confirmed';
      ajax_service.post_obj(url + 'issue_to_maintanance', issue_update_data).then(function (result) {
        if (result.message == 'success') {
          $scope.assigned = result;
          // $ionicLoading.hide();
          myPopup = $ionicPopup.show({
            title: '<p class="text-center balanced">Issue Assigned Successfully</p>'
          });
          $timeout(function () {
            myPopup.close();
            $state.go('app.home_page');
          }, 2000);
        }
      });
    };
    $scope.showPopup = function (issue_id, status) {
      data = {};
      data.token = token;
      data.user_id = user.id;
      ajax_service.post_obj(url + 'getemployeeList', data).then(function (response) {
        $scope.emp_list = response.data;
      });
      $scope.issue_id = issue_id;
      $scope.status = status;
      myPopup = $ionicPopup.show({
        template: ' <label class="item item-input item-select "><div class="input-label"></div><select class="col-50 txt_2" ng-model="emp_id" required name="Select Option" ng-options="item.id as item.name for item in emp_list"> <option  disabled selected value="" >Select Option</option></select></label><button class="button button-positive button-small hed_46" ng-click="issue_change(issue_id,status,emp_id)">Submit</button><button class="button button-positive button-small hed_46" ng-click="close()">cancel</button>',
        title: 'Select Assistant',
        scope: $scope,
      });
    };
    $scope.close = function () {
      myPopup.close();
    };
    $scope.issue_change = function (issue_id, status, emp_id) {
      myPopup.close();
      //
      // $ionicLoading.show({
      //   template: '<ion-spinner icon="lines" class="spinner-energized"></ion-spinner> <br/>Loading'
      // });
      issue_data = {};
      issue_data.token = token;
      issue_data.issue_id = issue_id;
      issue_data.issue_status = status;
      issue_data.user_id = user.id;
      issue_data.prop_head_assist_id = emp_id;

      ajax_service.post_obj(url + 'issue_tracking_store', issue_data).then(function (response) {
        // $ionicLoading.hide();
        myPopup = $ionicPopup.show({
          title: '<p class="text-center balanced">Issue Assigned To Assistant Successfully</p>'
        });
        $timeout(function () {
          myPopup.close();
          $state.go('app.home_page');
        }, 2000);
      });
    };

  })
  .controller('PhataskCtrl', function ($scope, $ionicModal, ajax_service, $state) {
    $scope.maintenance_issue_list = function () {
      data = {};
      data.token = token;
      data.user_id = user.id;
      ajax_service.post_obj(url + 'maintenance_issue_list', data).then(function (result) {
        if (result.status == 'success') {
          if(result.data.length > 0){
          $scope.maintenance_list = result.data;
          }else{
           $scope.emptyimg=true;
           }
        }
      });
    };
    $scope.maintenance_issue_list();
  })
  .controller('detail_to_main', function ($scope, $ionicModal,modal_service,ajax_service, $stateParams, $ionicPopup, $ionicLoading,$ionicModal,$state, $timeout) {
    var issue_id = $stateParams.issue_id;
    $scope.img_url = img_url;
    $scope.maintenance_issue_list = function (issue_id) {
      data = {};
      data.token = token;
      data.issue_id = issue_id;
      data.user_id = user.id;
      ajax_service.post_obj(url + 'maintenance_issue_list', data).then(function (result) {
        if (result.status == 'success') {
          console.log(result);
          $scope.maintenance_list = result.data;
          $scope.issue_video = result.data.issue_video;
        }
      });
    };
    $scope.maintenance_issue_list(issue_id);
    $scope.issue_change = function (issue_id, status, reason) {
      console.log(issue_id, status, reason)
      // $ionicLoading.show();
      issue_data = {};
      issue_data.token = token;
      issue_data.issue_id = issue_id;
      issue_data.issue_status = status;
      issue_data.user_id = user.id;
      if (reason != null) {
        myPopup.close();
        issue_data.reason = reason;
      }
      issue_data.master_type = "tech";

      ajax_service.post_obj(url + 'issue_tracking_store', issue_data).then(function (response) {
        $scope.issue = response.data;
        // $ionicLoading.hide();
         myPopup = $ionicPopup.show({
          title: '<p class="text-center balanced ">Message Send Successfully</p>'
        });
        $timeout(function () {
          myPopup.close();
          $state.go('app.home_page');
        }, 2000);
      });
    };
    var myPopup = "";
    $scope.showPopup = function (issue_id, status) {
      if (status == 'Issue Rejected By Technician' || status == 'Quote Work Rejected By Technician') {
        $scope.status = status;
        $scope.issue_id = issue_id;
        myPopup = $ionicPopup.show({
          // template: ' <div class="hef">Description</div><br><textarea rows="6" cols="40" placeholder="Description" ng-model="category.description" ng-class="err.description" ></textarea><button class="button button-positive button-small hed_46 " ng-click="issue_change(issue_id,status,reason)">Submit</button><button class="button button-positive button-small hed_46" ng-click="close()">cancel</button>',
          template: '<ion-list><ion-radio ng-model="reason" value="longdistance">Long Distance</ion-radio><ion-radio ng-model="reason" value="busy">Busy</ion-radio></ion-list><button class="button button-positive button-small hed_46 " ng-click="issue_change(issue_id,status,reason)">Submit</button><button class="button button-positive button-small hed_46" ng-click="close()">cancel</button>',
          title: 'Reason For Decline',
          scope: $scope,
        });
      } else {
        $scope.issue_change(issue_id, status);
      }
    };
    $scope.close = function () {
      myPopup.close();
    };
    $scope.showImages = function (index, images) {
      $scope.activeSlide = index;
      $scope.images = images;
      options = {
        scope: $scope
      };
      modal_service.from_url('templates/type.html', options).then(function (modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    };
    $scope.closeModal = function () {
      $scope.modal.hide();
      $scope.modal.remove();
    };
  })
  .controller('issue_assign_ctrl', function ($scope, $stateParams, ajax_service, $state, $ionicPopup) {
    var issue_id = $stateParams.issue_id;
    $scope.choice = '';
    ajax_service.post_obj(url + 'maintenance_list', {
      token: token,
      user_id: user.id
    }).then(function (result) {
      if (result.status == 'success') {
        $scope.maintenance_list = result.data;
      }
    });
    $scope.assign_issue = function (maintenance_id) {
      data = {};
      data.token = token;
      data.user_id = user.id;
      data.issue_id = issue_id;
      data.maintenance_id = maintenance_id;
      ajax_service.post_obj(url + 'issue_detail', data).then(function (result) {
        if (result.status == 'success') {
          $ionicPopup.show(result.message).then(function () {
            $state.go('app.home_page');
          });
        }
      });
    };
    $scope.select_maintenance = function (item) {
      $scope.choice = item.id;
    };
  })

  .controller('forgotPasswordCtrl', function ($scope, ajax_service, $stateParams, $cordovaToast, $state) {
    $scope.step1 = true;
    id = '';
    user_id = '';
    $scope.getotp = function (data) {
      data.token = token;
      data.page = 'send_otp';
      data.user_id = data.user_id;
      user_id = data.user_id;
      this.data = {};
      // alert(JSON.stringify(user_id));

      ajax_service.post_obj(url + 'sendotp', data).then(function (result) {
        if (result.status == 'success') {
          id = result.data.id;
          $cordovaToast.showShortBottom(result.message);
          $scope.step1 = false;
          $scope.step2 = true;
        } else {
          // $cordovaToast.showShortBottom(result.message);
          $cordovaToast.showShortTop(result.message);
        }
      });
    };
    $scope.resend_otp = function () {
      data = {};
      data.token = token;
      data.user_id = user_id;
      this.data = {};
      ajax_service.post_obj(url + 'sendotp', data).then(function (result) {
        if (result.status == 'success') {
          id = result.data.id;
          $scope.step1 = false;
          $scope.step2 = true;
        }
      });
    };
    $scope.otp_verification = function (data) {
      data.token = token;
      data.id = id;
      this.data = {};
      ajax_service.post_obj(url + 'verifyotp', data).then(function (result) {
        if (result.status == 'success') {
          $scope.step1 = false;
          $scope.step2 = false;
          $scope.step3 = true;
        } else {
          $cordovaToast.showShortBottom(result.message);
        }
      });
    };
    $scope.change_password = function (data) {
      data.token = token;
      data.id = id;
      this.data = {};
      if (data.new_password == data.re_password) {
        ajax_service.post_obj(url + 'userrepwd', data).then(function (result) {
          if (result.status == 'success') {
            $cordovaToast.showShortBottom('Password Changed Successfully');
            $state.go('login');
          } else {
            $cordovaToast.showShortBottom(result.message);
          }
        });
      } else {
        $cordovaToast.showShortBottom('Password Mismatched');
      }
    };
  })
  // .controller('OpenCtrl', function ($scope) {
  // })
  .controller('inspection_ctrl', function ($scope, $cordovaToast, camera_actions, $ionicLoading, ajax_service, $ionicModal,modal_service, $ionicSlideBoxDelegate, $ionicPopup, $timeout, $ionicActionSheet, $cordovaCamera, $cordovaCapture, $ionicScrollDelegate, $cordovaFileTransfer) {
    $scope.inspection = {};
    initial_config = function () {
      data = {};
      data.token = token;
      ajax_service.post_obj(url + 'inspecton_categories', data).then(function (result) {
        if (result.status == 'success') {
          $scope.categories = result.data;
          $scope.categories.forEach(function (value) {
            value.condition = '';
            value.description = '';
          });
        }
      });

    };
    property=function(){
      data = {};
      data.token = token;
      data.user_id = user.id;
      ajax_service.post_obj(url + 'property_name', data).then(function (result) {
        console.log(result);
          $scope.property_name_list = result;
      });
    }
    property();
    initial_config();
    $scope.property_show = function () {
      data = {};
      data.token = token;
      data.user_id = user.id;
      ajax_service.post_obj(url + 'property_name', data).then(function (result) {
        console.log(result);
          $scope.property_name_list = result;
      });
      options = {
        scope: $scope
      };
      modal_service.from_url('templates/property_show.html', options).then(function (res) {
        $scope.modal = res;
        console.log(res);
        $scope.modal.show();
      });
    };

    $scope.property_detail_single=function(data){

      console.log(data);
      $scope.bind=data;
      $scope.closeModal();
    }
    $scope.showImages = function (index, images) {
      $scope.activeSlide = index;
      $scope.images = images;
      options = {
        scope: $scope
      };
      modal_service.from_url('templates/type.html', options).then(function (modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    };
    $scope.closeModal = function () {
      $scope.modal.hide();
      $scope.modal.remove();
    };
    $scope.image_a = [];
    $scope.showActionsheet = function () {
      $ionicActionSheet.show({
        titleText: 'ActionSheet Example',
        buttons: [{
          text: '<i class="icon ion-camera "></i> Camera'
        },
        {
          text: '<i class="icon ion-android-image"></i> Gallery'
        },
        ],
        cancelText: 'Cancel',
        cancel: function () { },
        buttonClicked: function (index) {
          if (index == 0) {
            $scope.getPicture('camera');
          }
          if (index == 1) {
            $scope.getPicture('file');
          }
          return true;
        },
      });
    };
    $scope.getPicture = function (type) {
      camera_actions.selImages(type).then(function (imageUri) {
        if (imageUri != undefined) {
          $scope.image_a.push(imageUri);
        }
      });
    };
    $scope.remove = function (item) {
      var index = $scope.image_a.indexOf(item);
      $scope.image_a.splice(index, 1);
    };
    $scope.video = '';
    $scope.captureVideo = function () {
      $cordovaCapture.captureVideo(video_option).then(function (videoData) {
        $scope.video = videoData[0].fullPath;
      }, function (err) {
        alert('err' + JSON.stringify(err));
      });
    };


    $scope.inspection_store = function (inspection, categories, image_a, video,property_id) {

      inspection.property_id = property_id;
      inspection.categories = categories;
      inspection.token = token;
      inspection.user_id = user.id;
      console.log(inspection);
      // $ionicLoading.show();
      if (video) {
        vid_params = {
          path: 'issue',
          type: 'videos',
        };
        camera_actions.file_upload(url + 'upload_file', video, vid_params).then(function (res) {
          if (res.status == 'success') {
            inspection.video = res.response;
            condition_check(image_a, inspection);
          } else {
            $cordovaToast.showShortBottom(res.msg);
          }
        });
      } else {
        if(image_a.length > 0 ){

          condition_check(image_a, inspection);
        }else{
          $scope.image=true;
          // $ionicLoading.hide();
        }
      }
    };

    var condition_check=function(image_a, inspection){
      count=[];
      count_1=[];
      angular.forEach(inspection.categories, function (values) {
        if(values.condition !=""){
           if(values.condition == "damage"){
              if(values.description == ""){
                  count_1.push(values);
              }
           }
        }else{
          count.push(values);
        }
        })

      if(count.length>0){
        // $ionicLoading.hide();
        myPopup = $ionicPopup.show({title: '<p class="assertive">Please fill all mandatory fields</p>'});
        $timeout(function () { myPopup.close();}, 3000);
      }else if(count_1.length>0){
        // $ionicLoading.hide();
        myPopup = $ionicPopup.show({title: '<p class="assertive">Description field is required</p>'});
        $timeout(function () { myPopup.close();}, 3000);
      }
      else{
        inspection_store_process(image_a, inspection);
      }
    }
    var inspection_store_process = function (image_a, inspection) {

      file_ids = [];
      img_params = {
        path: 'inspections',
        type: 'photos',
      };
      angular.forEach(image_a, function (value) {
        camera_actions.file_upload(url + 'upload_file', value, img_params).then(function (res) {
          if (res.status == 'success') {
            file_ids.push(res.response);
          }else{
            myPopup = $ionicPopup.show({
              title: '<p class="text-center assertive">Network Error !! Try after sometime!!</p>'
            });
            $timeout(function () {
              myPopup.close();
          // $ionicLoading.hide();
            }, 3000);
          }

          inspection.imgs = file_ids;

          if (image_a.length == file_ids.length) {
            store_inspection(inspection);
          }
        });
      });
    };
    var store_inspection = function (inspection) {
      ajax_service.post_obj(url + 'inspection_store', inspection).then(function (response) {
        // $ionicLoading.hide();
      // alert(JSON.stringify(response));

        if (response.status == 'success') {
          $scope.inspection = {};
          $scope.bind = {};
          $scope.image_a = [];
          $scope.video = '';
          initial_config();
          myPopup = $ionicPopup.show({
            title: '<p class="balanced">Inspection Stored Successfully</p>'
          });
        }else{

          myPopup = $ionicPopup.show({
            title: '<p class="assertive">Please Fill All Mandatory Fields</p>'
          });
        }
        $timeout(function () { myPopup.close(); }, 3000);

      });
    };
    $scope.clear = function () {
      // $scope.category.condition='';
      $scope.inspection = {};
      $scope.image_a = [];
      $scope.video = '';
      initial_config();

    };
  })
.controller('InsCtrl', function($scope,$timeout,$ionicScrollDelegate,modal_service,ajax_service,$ionicModal,$ionicPopover,$state,$ionicPlatform) {
  $scope.stop_loadmore = true;
  $scope.current_page_no = 0;

  $scope.refresh=function(){
    $scope.field="";
    $scope.inspection(1);
  $scope.stop_loadmore = true;
  }
  $scope.domore=function(){
    $scope.current_page_no += 1;
    $scope.inspection($scope.current_page_no);
  }

 $scope.inspection=function(page_no){
    data = {};
    data.token = token;
    data.user_id = user.id;
    data.items_per_pages=5;
    data.current_page_no=page_no;

    ajax_service.post_obj(url+'inspection_list',data).then(function(result){
      console.log(result.data);
      if(result.data.length > 0){
        $scope.show_empty=false;
          if (page_no  == 1) {
            $scope.list = result.data;
            } else {
          $scope.list = $scope.list.concat(result.data);
          }
          $scope.$broadcast('scroll.refreshComplete');
          $scope.$broadcast('scroll.infiniteScrollComplete');
      }else if(page_no  == 1){
        $scope.emptyimg=true;
        $scope.show_empty=true;
      }else{
        $scope.stop_loadmore=false;
      }
    })
  }

  $scope.search_field=function(value){
    $ionicScrollDelegate.scrollTop();
    data={}
    data.search=value;
    data.token=token;
    data.user_id = user.id;
    if(!value){
    data.items_per_pages=5;
    data.current_page_no=1;
    }
        ajax_service.post_obj(url + 'inspection_list', data).then(function (result) {
              if(result.data.length > 0){
                $scope.list = result.data;
                $scope.show_empty=false;

              }
              else{
                $scope.list ="";
                $scope.show_empty=true;
              }
            });
  }
// inspection();


    $scope.showModal = function (id) {
      data={};
      data.id=id;
      ajax_service.post_obj(url+'inspection_report_list',data).then(function(result){
        console.log(result);
        $scope.item_list=result.insp_report;
        $scope.inspection=result.inspection;
        $scope.image_detail=result.image_detail;
        if(result.video){
          $scope.vid = result.video;
          $scope.vid_fn = img_url + $scope.vid.image_path  +'/'+ $scope.vid.image_name;
console.log($scope.vid_fn);
        }
      })
      $ionicModal.fromTemplateUrl('templates/director/ins_modal.html', {
        scope: $scope,
        hardwareBackButtonClose: false
      }).then(function (modal) {
        $scope.modal2 = modal;
        $scope.modal2.show();
      });
    };
  // }
  $scope.zoomMin = 1;
  $scope.showImages = function (index, images) {
    $scope.activeSlide = index;
    $scope.images = images;
    options = {
      scope: $scope
    };
    modal_service.from_url('templates/type.html', options).then(function (modal) {
      $scope.modal = modal;
      $scope.modal.hardwareBackButtonClose = false;
      $scope.modal.show();
    });
  };
  $scope.closeModal = function () {
    $scope.modal.hide();
    $scope.modal.remove();
  };
  $scope.closeModal2 = function () {
    $scope.modal2.hide();
    $scope.vid = "";
    $scope.modal2.remove();
  };
 });
