angular.module('starter.controllers1', [])
.controller('work_start', function($scope,$state,Upload,$ionicLoading,$ionicBackdrop,master_data,ajax_service,$stateParams,$ionicModal,$ionicSlideBoxDelegate,$ionicPopup,$timeout,$ionicActionSheet,$cordovaCamera,$cordovaProgress,$cordovaCapture,$ionicScrollDelegate,$cordovaFileTransfer) {

   $scope.work_detail=function(){
    data = {};
    data.token = token;
    data.user_id = user.id;
    ajax_service.post_obj(url+'issues_list',data).then(function(result){
        console.log(result);
    if(result.length > 0){
      $scope.work=result;
    }else{
     $scope.emptyimg=true;
     }
       });
   };
   $scope.work_detail();

})
.controller('work_start_detail', function(location,$scope,camera_actions,$state,Upload,$ionicLoading,$ionicBackdrop,master_data,ajax_service,$stateParams,$ionicModal,$ionicSlideBoxDelegate,$ionicPopup,$timeout,$ionicActionSheet,$cordovaCamera,$cordovaProgress,$cordovaCapture,$ionicScrollDelegate,$cordovaFileTransfer) {
    var issue = $stateParams.issue_id;
    $scope.shown=true;

    $scope.work_detail_get=function(issue){
      console.log(issue);

        data = {};
        data.token = token;
        data.user_id = user.id;
        data.issue_id = issue;
        ajax_service.post_obj(url+'work_detail_get',data).then(function(result){
            console.log(result);
        $scope.task=result.data;

        console.log( $scope.task.params);
        if($scope.task.params == 1){
            $scope.buttonshow1 = false;
            $scope.buttonshow2 = true;
            //  $scope.shown=true;
            // $scope.buttonshow3 = true;

        }else if($scope.task.params == 2){
            // $scope.shown=false;
            $scope.buttonshow1 = true;
            $scope.buttonshow2 = false;
            $scope.buttonshow3 = false;

        }
        if($scope.task.camera == 1){
            $scope.camerashow=false;
        }else{
          $scope.camerashow=true;

        }
        if($scope.task.hide == 3){
            $scope.buttonshow3 = false;
        }else{
            $scope.buttonshow3 = true;

        }
           });
       };
       $scope.work_detail_get(issue);

       $scope.zoomMin = 1;
       $scope.showImages = function (index, images) {
         $scope.activeSlide = index;
         $scope.images = images;
         options = {
           scope: $scope
         };
         modal_service.from_url('templates/type.html', options).then(function (modal) {
           $scope.modal = modal;
           $scope.modal.show();
         });
       };
       $scope.closeModal = function () {
         $scope.modal.hide();
         $scope.modal.remove();
       };

       $scope.tech_work_time = function (status) {
        $scope.shown=false;
        if(status == 1){
            $status=($scope.task.issue_status == "Quote Work Accepted By Technician") ? ($status="Quote Work start"):($status="Issue Work start");
         }
        else{
            $status=($scope.task.issue_status == "Quote Work Accepted By Technician") ? ($status="Quote Work pause"):($status="Issue Work pause");
        }
console.log($status);

        issue_data = {};
        issue_data.token = token;
        issue_data.issue_id =$scope.task.issue_id;
        issue_data.issue_status =$status;
        issue_data.user_id = user.id;

       ajax_service.post_obj(url+'issue_tracking_store', issue_data).then(function (response) {
        $scope.issue = response.data;
        $scope.work_detail_get(issue);
        console.log($scope.issue);
       });
       };

        $scope.submit=function(parms){
        data = {};
        data.token = token;
        data.user_id = user.id;
        data.issue_status=$scope.task.issue_status;
        data.issue_id=$scope.task.issue_id;
        data.parms=parms;
        // console.log($scope.task.issue_status);

        ajax_service.post_obj(url+'work_timing_send',data).then(function(result){
            console.log(result);
            $scope.work_detail_get(issue);
        $scope.task=result.data.start_time;
           });
       };

  $scope.location_get=function(){
    location.get_coords().then(function (res) {
      // alert(JSON.stringify(res));
      if(res.status == "success"){

      // alert(JSON.stringify(res));
      $scope.coords_data = res;
      data = {};
      data.token = token;
      data.user_id = user.id;
      data.lat = res.data.lat;
      data.lng = res.data.lng;
      data.date = res.date;
      data.time = res.data.timestamp;
      ajax_service.post_obj(url+'location_share',data).then(function(result){
        console.log(result);
       });
      // fb.push('rapro/'+user.id+'/'+coords_data.date+'/location_history',coords_data.data);
      // if (user.admin_role_id == 9 || user.admin_role_id == 6) {
      //   fb.set('rapro/' + user.id + '/' + coords_data.date + '/current_location', coords_data.data);
         }
         else{
      // alert(JSON.stringify(res));
      myPopup = $ionicPopup.show({
        title: '<p class="">Somthing Went Wrong...  Try again </p>'
      });
      $timeout(function () {
        myPopup.close();
      }, 2000);
         }
    });
  };

  $scope.showActionsheet = function () {
    $ionicActionSheet.show({
      titleText: 'ActionSheet Example',
      buttons: [{
        text: '<i class="icon ion-camera "></i> Camera'
      },
      {
        text: '<i class="icon ion-android-image"></i> Gallery'
      },
      ],
      cancelText: 'Cancel',
      cancel: function () { },
      buttonClicked: function (index) {
        if (index == 0) {
          $scope.getPicture('camera');
        }
        if (index == 1) {
          $scope.getPicture('file');
        }
        return true;
      },
    });
  };

  $scope.image_a=[];
  $scope.img_data=[];


      $scope.getPicture = function (type) {
        camera_actions.selImages(type).then(function (imageUri) {
          if (imageUri != undefined) {
            $scope.image_a.push(imageUri);
          }
        });
      };

      $scope.remove = function(item) {
        var index= $scope.image_a.indexOf(item);
        $scope.image_a.splice(index, 1);
      };
     $scope.submit_work=function(photos,issue_id){
      // $ionicLoading.show();
      issue={};
      issue.master_type = "tech";
      // console.log(photos);
        if (photos.length == 0) {
          // alert();
          issue.token = token;
          issue.user_id = user.id;
          issue.issue_id=issue_id;
          issue.issue_status="work completed";
          ajax_service.post_obj(url + 'issue_tracking_store', issue).then(function (response) {
            submit_res(response);
          });
        }else{
          file_ids = [];
            img_params = {
              path: 'issue',
              type: 'photos',
            };
            angular.forEach(photos,function(value){
              camera_actions.file_upload(url + 'upload_file', value, img_params).then(function (res) {

                  file_ids.push(res.response);
                  if (photos.length == file_ids.length) {
                    issue.token = token;
                    issue.user_id = user.id;
                    issue.issue_id=issue_id;
                    issue.issue_status="work completed";
                    issue.imgs = file_ids;
                    ajax_service.post_obj(url +'issue_tracking_store', issue).then(function (response) {
                    //  alert(JSON.stringify(response));
                      submit_res(response);
                    });
                  }
                });
            })
          }
      };
        var submit_res=function(response){
            // $ionicLoading.hide();
          if (response.status == 'success') {
            $scope.issue = {};
            $scope.image_a = [];
            myPopup = $ionicPopup.show({
              title: '<p class="text-center balanced">Thank You </p>'
            });
            $timeout(function () {
              myPopup.close();
              $state.go('app.home_page');
              }, 2000);

          }
 }
$scope.showModal = function (templateUrl) {
  $ionicModal.fromTemplateUrl(templateUrl, {
    scope: $scope
  }).then(function (modal) {
    $scope.modal = modal;
    $scope.modal.show();
  });
};
$scope.closeModal = function () {
  $scope.modal.hide();
  $scope.modal.remove();
};
$scope.showImages = function (index, images) {
  $scope.activeSlide =index;
  $scope.images = images;
  $scope.showModal('templates/type.html');
};

 $scope.showStatus = function() {
 myPopup = $ionicPopup.show({
     title: 'Status',
     template: '<ion-list><ion-checkbox ng-model="filter.red"> Tenant not available</ion-checkbox><ion-checkbox ng-model="filter.yellow">  Technical issue</ion-checkbox> <ion-checkbox ng-model="filter.Blue">Power cut</ion-checkbox><ion-checkbox ng-model="filter.pink"> Late Arrival</ion-checkbox></ion-list><button class="button button-positive button-small hed_46" >Submit</button><button class="button button-positive button-small hed_46" ng-click="close()">cancel</button>',
      scope: $scope,
   });

   statusPopup.then(function(res) {
     if(res) {
       console.log('You are sure');
     } else {
       console.log('You are not sure');
     }
   });
 };
    $scope.close = function () {
      myPopup.close();
    };
})

.controller('ManagerissueCtrl',function($scope,ajax_service,$timeout, $ionicPopup,$interval,$state) {

  $scope.issues_list = function () {
    issue_data = {};
    issue_data.token = token;
    issue_data.user_id = user.id;
      ajax_service.post_obj(url + 'issues_list_director', issue_data).then(function (result) {
      if(result.data.length > 0){
        $scope.issues = result.data;
      }else{
       $scope.emptyimg=true;
       }
       $timeout( function() {
        $scope.$broadcast('scroll.refreshComplete');
      }, 1000);
      });
  };
 $scope.issues_list();
  })
  .controller('ManageCtrl', function($ionicModal,$scope,ajax_service,$stateParams,$state, $ionicPopup) {
    var issue_id = $stateParams.id;
 $scope.img_url=img_url;
    $scope.maintenance_issue_list = function (issue_id) {
      data = {};
      data.token = token;
      data.issue_id = issue_id;
      data.user_id = user.id;
      ajax_service.post_obj(url + 'admin_issue_list', data).then(function (result) {
        console.log(result);
        if (result.status == 'success') {
          $scope.issue_list = result.data;
        console.log($scope.issue_list);

          // $scope.issue_video = result.data.issue_video;
        }
      });
    };
    $scope.maintenance_issue_list(issue_id);

    $scope.showModal = function (templateUrl) {
      // console.log($scope);
      $ionicModal.fromTemplateUrl(templateUrl, {
        scope: $scope
      }).then(function (modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    };
    $scope.closeModal = function () {
      $scope.modal.hide();
      $scope.modal.remove();
    };
      $scope.showImages = function (index, images) {
        $scope.activeSlide = index;
        $scope.images = images;
        $scope.showModal('templates/type.html');
      };
    $scope.showAlert = function() {

      var alertPopup = $ionicPopup.alert({
        title: 'Image',
        template: '<img src="images/paint.jpg" >'
      });

      alertPopup.then(function(res) {
        if(res) {
          console.log('You are sure');
        } else {
          console.log('You are not sure');
        }
      });
    };
    $scope.videoAlert = function(video) {

      console.log(img_url+video);
      $scope.urls=img_url+video;
      myPopup = $ionicPopup.show({
        title: 'Video ',scope:$scope,
       template: '<i class="button ion-close-circled butn_delt_1 "  ng-click="close()"></i> <video class="padding" src="{{urls |trustUrl }}" controls="true" width="230" height="150" > </video> '
      });
    };
    $scope.close = function () {
      myPopup.close();
    };
      $scope.historyAlert = function(issue_id) {

        ajax_service.post_obj(url + 'get_issue_tracking', {'issue_id':issue_id}).then(function (response) {
          console.log(response);
          if (response.status == 'success') {
            $scope.issue_trace_list = response.data;
            $scope.property = response.property;
            $scope.showModal('templates/director/issuehistpry.html');
            }
        });

    };

       $scope.issueAlert = function() {
      var alertPopup  = $ionicPopup.alert({
        title: 'Property ',
       template: '<div class="card"><div class="item item-divider">Status : Issue Confirmed</div><div class="item item-text-wrap">Approved by : Property Head Assistant</div></div> <div class="card"><div class="item item-divider">Status : Issue Confirmed</div><div class="item item-text-wrap">Approved by : Property Head Assistant</div></div> <div class="card"><div class="item item-divider">Status : Issue Confirmed</div><div class="item item-text-wrap">Approved by : Property Head Assistant</div></div>'
      });

       alertPopup.then(function(res) {
      });
    };
  })
.controller('CustomeListCtrl', function ($scope,$timeout,$ionicScrollDelegate,$ionicPopup, $ionicBackdrop, ajax_service, $state, $stateParams, $ionicModal, $ionicSlideBoxDelegate, $cordovaFile, $cordovaFileTransfer, $cordovaDevice) {
  $scope.stop_loadmore = true;
  $scope.current_page_no = 0;

$scope.refresh=function(){
  $scope.field="";
  $scope.customer_load(1);
  $scope.stop_loadmore = true;
}
$scope.domore=function(){
  $scope.current_page_no += 1;
  $scope.customer_load($scope.current_page_no);
}
$scope.customer_load = function(page_no) {
  console.log(page_no);
  data = {};
  data.token = token;
    data.items_per_pages=5;
    data.current_page_no=page_no;

  ajax_service.post_obj(url + 'customer_list', data).then(function (result) {
    if(result.data.status="success"){
      if(result.data.length > 0){
        if (page_no  == 1) {
            $scope.customer_list = result.data;
            } else {
           $scope.customer_list = $scope.customer_list.concat(result.data);
           }
           $scope.$broadcast('scroll.refreshComplete');
          $scope.$broadcast('scroll.infiniteScrollComplete');
      }else if(page_no  == 1){
       $scope.emptyimg=true;
      }else{
       $scope.stop_loadmore=false;
      }
    }

      });
};
$scope.search_field=function(value){
  $ionicScrollDelegate.scrollTop();
  data={}
  data.search=value;
  data.token=token;
      if(!value){
  data.items_per_pages=5;
  data.current_page_no=1;
      }
      ajax_service.post_obj(url + 'customer_list', data).then(function (result) {
            if(result.data.length > 0){
              $scope.customer_list = result.data;
            }
            else{
              $scope.customer_list ="";
              $scope.show_empty=true;
            }
          });
}
 $scope.customer_details = function (id) {
   $state.go('app.cus_info', {
     customer_id: id
   });
 };

 })
.controller('CustomerInfoCtrl', function ($scope, $ionicBackdrop, ajax_service, $state, $stateParams, $ionicModal, $ionicSlideBoxDelegate, $cordovaFile, $cordovaFileTransfer, $cordovaDevice) {
  var customer_id = $stateParams.customer_id;
  var detail_view = function () {
    data = {};
    data.token = token;
    data.customer_id = customer_id;
    ajax_service.post_obj(url + 'customer_details', data).then(function (result) {
      if (result.status == 'success') {
        $scope.customer_details = result.data;
      }
    });
  };
  detail_view();
  $scope.showModal = function (templateUrl) {
    $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  };
  $scope.closeModal = function () {
    $scope.modal.hide();
    $scope.modal.remove();
  };
  $scope.showImages = function (index, images) {
    $scope.activeSlide = index;
    $scope.images = images;
    $scope.showModal('templates/type.html');
  };
})
.controller('AttendanceCtrl', function ($ionicModal, $timeout, $scope, ajax_service) {
  var attendance=function(){
    data = {};
    data.token = token;
    data.user_id = user.id;
     ajax_service.post_obj(url + 'attendance', data).then(function (result) {
       console.log(result.data.data);
      if(result.data.data.length > 0){
        $scope.list = result.data.data;
      }else{
       $scope.emptyimg=true;
          }
          $timeout( function() {
            $scope.$broadcast('scroll.refreshComplete');
          }, 1000);
     });
  }
  attendance();
  })

 .controller('notification_ctrl', function ($ionicModal, $scope, ajax_service, $stateParams, $state, $ionicPopup) {
        $scope.notifications_list = function(){
         data = {};
        data.token = token;
        data.user_id = user.id;
         ajax_service.post_obj(url + 'notifications', data).then(function (result) {
          if (result.status == 'success') {
            console.log(result);
            $scope.notifications = result.data;
          }
        });

         $scope.fun=function(id){
          data={};
               data.id=id;
            data.user_id = user.id;
              ajax_service.post_obj(url + 'notifications_status', data).then(function (result) {
                if (result.status == 'success') {
                  console.log(result);
                  $scope.notifications = result.count;
                  // $scope.notifications_list();
                }
              });

         }
       };
       $scope.notifications_list();
            })

  .controller('issue_master_ctrl', function ($scope,$timeout,ajax_service, $ionicPopover,$state) {
    if (user.admin_role_id == 5 || user.admin_role_id == 6) {
      $scope.buttonshow = true;
    } else {
      $scope.buttonshow = false;
      $scope.buttonhide = true;
    }

    $ionicPopover.fromTemplateUrl('templates/issuebtns.html', {
      scope: $scope,
    }).then(function (popover) {
      $scope.popover = popover;
    });
    $scope.doSomething = function ($event) {
      $scope.popover.show($event);
    };
    $scope.gobuttons = function (norms) {
      if (norms == 1) {
        $state.go('app.create_issue');
        $scope.popover.hide();
      } else {
        $state.go('app.issue_status');
        $scope.popover.hide();
      }
    };

    $scope.property = function () {
      issue_data = {};
      issue_data.token = token;
      issue_data.user_id = user.id;
      if (user.admin_role_id == 6) {
            // issue_data.status = 'Waiting For Confirmation';
            issue_data.status = 'New';
          } else if (user.admin_role_id == 5) {
            issue_data.status = 'New';
          } else if (user.admin_role_id == 8) {
            issue_data.status = "Issue Confirmed";
          }
      ajax_service.post_obj(url + 'issue_master_list', issue_data).then(function (result) {
        if(result.data.length > 0){
        console.log(result);
          $scope.Issue_master = result.data;
         }else{
          $scope.emptyimg=true;
         }
         $timeout( function() {
          $scope.$broadcast('scroll.refreshComplete');
        }, 1000);
      });
    };
    $scope.property();
  })
  .controller('tenant_list', function ($scope,$timeout,ajax_service, $ionicPopover,$state,$ionicScrollDelegate) {
    $scope.stop_loadmore = true;
    $scope.current_page_no = 0;
    $scope.current_page_no_2 = 0;
    $scope.card="vacant";

    $scope.cardss=function(no){
      console.log(no);
     $scope.card=(no == 1) ?  "vacant" : "occupied";
     $scope.current_page_no = 1;
     $scope.current_page_no_2 = 1;
     $ionicScrollDelegate.scrollTop();
     $scope.tenant_vac(1,$scope.card);
    }
  $scope.refresh=function(categories){
    console.log(categories);
  $scope.tenant_vac(1,categories);
}
$scope.domore=function(group_by){
  console.log(group_by);

  $scope.card=(group_by) ? group_by : "vacant";

  if(group_by == "occupied"){
    $scope.current_page_no += 1;

    $scope.tenant_vac($scope.current_page_no,group_by);
  }else{
    $scope.current_page_no_2 += 1;

    $scope.tenant_vac($scope.current_page_no_2,'vacant');

  }

}
    $scope.tenant_vac=function(page_no,group_by){
      console.log(page_no,group_by);
      data={};
      $scope.card=group_by;
      data.token = token;
      data.items_per_pages=5;
      data.current_page_no=page_no;
      data.value=group_by;
      ajax_service.post_obj(url+'list_of_tenant',data).then(function(result){
        console.log(result);
        $scope.occ=result.occupied_count;
        $scope.vac=result.vacant_count;
        if(result.status="success"){
          $scope.vacant=true;
          if(result.list.length > 0){
           $scope.emptyimg=false;

            if(group_by == "vacant"){
             console.log('vacant');
              $scope.show_repeat=true;
              $scope.show_repeat_a=false;

              if (page_no  == 1) {
                $scope.list=result.list;
                } else {
                 $scope.list = $scope.list.concat(result.list);
                 }
            }else{
            console.log('occupied');
            $scope.show_repeat=false;
            $scope.show_repeat_a=true;

            if (page_no  == 1) {
                $scope.list_oc=result.list;
                } else {

                 $scope.list_oc = $scope.list_oc.concat(result.list);

                 }
            }

              
               $scope.$broadcast('scroll.refreshComplete');
              $scope.$broadcast('scroll.infiniteScrollComplete');
          }else if(page_no  == 1){
           $scope.emptyimg=true;
          }else{
            console.log("i");
           $scope.stop_loadmore=false;
          }
        }
      })
    }


  })
