var model = {};
var device_token = '';
var get_token = '';
var url = 'https://rapromobileapp.com/api/';
var img_url = 'https://rapromobileapp.com/';
var firebase_config = {};
var user = {};
var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvcHJhbmF2YXMubXlsYXBvcmV0b2RheS5pblwvYXBpXC9tb2JpbGVhcHBcL3VzZXJfbG9naW4iLCJpYXQiOjE1NTIxMDU0NjYsIm5iZiI6MTU1MjEwNTQ2NiwianRpIjoiSTBiTlA4NzVNbGZjbXMwNiIsInN1YiI6MSwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.TO2OZ3OVtASAN7aw4VpfVOaLeIlTgWtY2E31ikb8sGY";
var search_limit = 10;
var video_option = {
  limit: 1,
  duration: 45,
  quality:1,
  ios_quality:'high'
};
var track_interval = '';
var interval_duration = 10000;
var senderID = 651332495613;
var app_id = 'om99MimzbId820GRKTw5YZPfv';
angular.module('starter', ['ionic','hm.readmore','plgn.ionic-segment','starter.services', 'starter.controllers', 'starter.controllers1', 'ngCordova','ionic.rating',
    'ngFileUpload', 'angular.filter', 'ion-datetime-picker','angularjs-datetime-picker','ionic-monthpicker', 'ionic.contrib.ui.hscrollcards', 'firebase'
  ])
  .run(function ($ionicPlatform,$ionicPopup, ajax_service,$ionicHistory,acepush,ConnectivityMonitor,$rootScope,$ionicLoading,$timeout) {
    $ionicPlatform.ready(function () {

      ajax_service.post_obj(url + 'firebase_config', {
       token: token
     }).then(function (res) {
       firebase_config = res.data;
       interval_duration = res.interval_duration;
       console.log(res);
       firebase.initializeApp(firebase_config);
     });
       if(ionic.Platform.isAndroid()){
     if (window.cordova && window.Keyboard) {
       window.Keyboard.hideKeyboardAccessoryBar(true);
     }
   }else{
     if (window.cordova && window.cordova.plugins.Keyboard) {
cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
cordova.plugins.Keyboard.disableScroll(true);
}
}

     if (window.StatusBar) {
       StatusBar.styleDefault();
     }
     $rootScope.$on('loading:show', function() {
       var isAndroid = ionic.Platform.isAndroid();
       if(isAndroid){
         spinner='<ion-spinner icon="lines" class="spinner-balanced"></ion-spinner>';
       }
       else{
         spinner='<ion-spinner icon="ios"  class="spinner-balanced"></ion-spinner>';
       }

         $ionicLoading.show({  template: spinner,
             duration: 2000,animation: 'fadeIn',
                   showDelay: 10})
       })

       $rootScope.$on('loading:hide', function() {
           $timeout(function () {
         $ionicLoading.hide();
       }, 1000);
       })
             if(ionic.Platform.isAndroid()){
cordova.plugins.diagnostic.requestRuntimePermissions(function(statuses){
   for (var permission in statuses){
       switch(statuses[permission]){
           case cordova.plugins.diagnostic.permissionStatus.GRANTED:
               console.log("Permission granted to use "+permission);
               break;
           case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
               console.log("Permission to use "+permission+" has not been requested yet");
               break;
           case cordova.plugins.diagnostic.permissionStatus.DENIED:
               console.log("Permission denied to use "+permission+" - ask again?");
               break;
           case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
               console.log("Permission permanently denied to use "+permission+" - guess we won't be using it then!");
               break;
       }
   }
}, function(error){
   console.error("The following error occurred: "+error);
},[
   cordova.plugins.diagnostic.permission.ACCESS_FINE_LOCATION,
   cordova.plugins.diagnostic.permission.ACCESS_COARSE_LOCATION,
   cordova.plugins.diagnostic.permission.CAMERA,
   cordova.plugins.diagnostic.permission.WRITE_EXTERNAL_STORAGE
]);
}
       ConnectivityMonitor.startWatching();

     model = ionic.Platform.device();

     if(ionic.Platform.isAndroid()){
     var push = PushNotification.init({
       "android": {
         "senderID": senderID,
          "forceShow":true
       }
     });
   } else{
     var push = PushNotification.init({ "ios": { "alert": "true", "badge": "true", "sound": "true", "clearBadge": true } });
   }
     push.on('registration', function (data) {
       AppToken = data.registrationId;
       get_token = JSON.parse(localStorage.getItem('AppToken'));
       if (get_token != AppToken || !get_token || get_token == '' || get_token == null) {
         localStorage.setItem('AppToken', JSON.stringify(AppToken));
         var obj = {
           'app_id': app_id,
           'device_token': AppToken,
           'model': model.model,
           'platform': model.platform,
           'status': true
         };
         acepush.notification(obj);
       }
     });


      push.on('notification', function (data) {
        var a = data.additionalData.openUrl.split("_");
      });
    });

    $ionicPlatform.registerBackButtonAction(function(e) {
      e.preventDefault();
      function showConfirm() {
      var confirmPopup = $ionicPopup.show({
      title : 'Exit Rapro ?',
      template : 'Are you sure you want to exit Rapro?',
      buttons : [{
      text : 'Cancel',
      type : 'button-balanced button-outline',
      }, {
      text : 'Ok',
      type : 'button-balanced',
      onTap : function() {

      ionic.Platform.exitApp();
      }
      }]
      });
      };
      if ($ionicHistory.backView()) {
      $ionicHistory.backView().go();
      } else {
      showConfirm();
      }
      return false;
      }, 101);
  })
   .filter("trustUrl", function ($sce) {
     return function (Url) {
       console.log(Url);
       return $sce.trustAsResourceUrl(Url);
     };
   })
   .filter("convertFileSrc", function () {
  return function (Url) {
    console.log(Url);
    if(ionic.Platform.isIOS()) {
     var image = window.Ionic.normalizeURL(Url);
    return image;
  }else{
      return Url;
  }
  };
})
  .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider,$httpProvider) {
    $ionicConfigProvider.backButton.text('').icon('ion-ios-arrow-back').previousTitleText(false);
    $ionicConfigProvider.navBar.alignTitle('center');
    $ionicConfigProvider.views.swipeBackEnabled(false);
    $httpProvider.interceptors.push(function($rootScope) {

     return {
       request: function(config) {
         $rootScope.$broadcast('loading:show')
         return config
       },
       response: function(response) {
         $rootScope.$broadcast('loading:hide')
         return response
       }
     }
   })
    $stateProvider
      .state('app', {
        url: '/app',
        cache: false,
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
      })
      .state('menu_welcome', {
        url: '/menu_welcome',
        cache: false,
        templateUrl: 'templates/menu_welcome.html',
        controller: 'DateCtrl'
      })
      .state('first_page', {
        url: '/first_page',
        templateUrl: 'templates/first_page.html',
      })
      .state('service', {
        url: '/service',
        templateUrl: 'templates/service.html',
        controller: 'ServiceCtrl'
      })
      .state('first_enquiry', {
        cache: false,
        url: '/first_enquiry',
        templateUrl: 'templates/first_enquiry.html',
        controller: 'NewenquryCtrl'
      })
     .state('login', {
        url: '/login',
        cache: false,
        templateUrl: 'templates/login.html',
        controller: 'AppCtrl'
      })
      .state('app.create_issue', {
        cache: false,
        url: '/create_issue/:id',
        views: {
          'menuContent': {
            templateUrl: 'templates/property_head/create_issue.html',
            controller: 'issue_create_ctrl'
          }
        }
      })
      .state('app.profile', {
        cache: false,
        url: '/profile',
        views: {
          'menuContent': {
            templateUrl: 'templates/profile.html',
            controller: 'ProfileCtrl'
          }
        }
      })

      .state('app.rent', {
        url: '/rent',
        views: {
          'menuContent': {
            templateUrl: 'templates/rent.html'
          }
        }
      })
      .state('app.leave_request', {
        cache: false,
        url: '/leave_request',
        views: {
          'menuContent': {
            templateUrl: 'templates/leave_request.html',
            controller: 'leave_management_ctrl'
          }
        }
      })
      .state('app.leave_approve', {
        cache: false,
        url: '/leave_approve',
        views: {
          'menuContent': {
            templateUrl: 'templates/leave_approve.html',
            controller: 'leave_management_ctrl'
          }
        }
      })
      .state('app.rent-history', {
        // cache: false,
        url: '/rent-history',
        views: {
          'menuContent': {
            templateUrl: 'templates/rent-history.html',
            controller: 'payment_history'
          }
        }
      })
      .state('app.status', {
        cache: false,
        url: '/status',
        views: {
          'menuContent': {
            templateUrl: 'templates/tenant/status.html',
            controller: 'StatusCtrl'
          }
        }
      })
      .state('app.payment', {
        cache: false,
        url: '/payment',
        views: {
          'menuContent': {
            templateUrl: 'templates/tenant/payment.html',
            controller: 'PaymentCtrl'
          }
        }
      })
      .state('app.notification', {
        url: '/notification',
        views: {
          'menuContent': {
            templateUrl: 'templates/notification.html',
          }
        }
      })
      .state('app.occupied_properties', {
        cache: false,
        url: '/occupied_properties',
        views: {
          'menuContent': {
            templateUrl: 'templates/property_head/occupied_properties.html',
            controller: 'property_head_ctrl'
          }
        }
      })
      .state('app.property_details', {
        url: '/property_details/:property_id',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/property_head/property_details.html',
            controller: 'property_details_ctrl'
          }
        }
      })
      .state('app.vacant_properties', {
        url: '/vacant_properties',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/property_head/vacant_properties.html',
            controller: 'property_head_ctrl'
          }
        }
      })
      .state('app.assigned_works', {
        url: '/assigned_works',
        views: {
          'menuContent': {
            templateUrl: 'templates/property_head/assigned_works.html',
            controller: 'property_head_ctrl'
          }
        }
      })
      .state('app.property', {
        url: '/property/:id',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/customer/property.html',
            controller: 'PropertyCtrl'
          }
        }
      })
      .state('app.property_detail', {
        url: '/property_detail/:id',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/customer/property_detail.html',
            controller: 'customer_PropertyCtrl'
          }
        }
      })

      .state('app.issue', {
        url: '/issue',
        views: {
          'menuContent': {
            templateUrl: 'templates/issue.html',
          }
        }
      })
      .state('app.issue_detail', {
        url: '/issue_detail',
        views: {
          'menuContent': {
            templateUrl: 'templates/issue-detail.html',
            controller: 'IssueDetailCtrl'
          }
        }
      })
      .state('app.exist_detail', {
        url: '/exist_detail',
        cache: false,
        views: {
          'menuContent': {
        templateUrl: 'templates/exist_detail.html',
        controller: 'exist_enquryCtrl'
            }
          }
        })
      .state('app.tech', {
        url: '/tech_detail',
        views: {
          'menuContent': {
            templateUrl: 'templates/tech.html',
          }
        }
      })
      .state('app.search1', {
        url: '/search1',
        views: {
          'menuContent': {
            templateUrl: 'templates/search1.html',
          }
        }
      })
      .state('app.issue_status', {
        url: '/issue_status',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/property_head/issue_status.html',
            controller: 'issue_status_ctrl'
          }
        }
      })
      .state('app.issue_master_list', {
        url: '/issue_master_list',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/property_head/issue_master_list.html',
            controller: 'issue_master_ctrl'
          }
        }
      })
      .state('app.issue_list', {
        url: '/issue_list/:issue_id',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/property_head/issue_list.html',
            controller: 'issue_ctrl'
          }
        }
      })
      .state('app.issue_details', {
        url: '/issue_details/:issue_id',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/property_head/issue_details.html',
            controller: 'IssueDetailCtrl'
          }
        }
      })
      .state('app.assign_to_maintenance', {
        url: '/assign_to_maintenance/:issue_id',
        views: {
          'menuContent': {
            templateUrl: 'templates/property_head/assign.html',
            controller: 'issue_assign_ctrl'
          }
        }
      })
      .state('app.issue_status_detail', {
        url: '/issue_status_detail',
        views: {
          'menuContent': {
            templateUrl: 'templates/property_head/issue_status_detail.html',
            controller: 'IssueDetailCtrl'
          }
        }
      })
      .state('app.expence', {
        url: '/expence',
        views: {
          'menuContent': {
            templateUrl: 'templates/customer/expence.html',
            controller: 'ExpenseCtrl'
          }
        }
      })

      .state('app.emp-issue', {
        url: '/emp-issue',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/technician/emp-issue.html',
            controller: 'PhataskCtrl'
          }
        }
      })
      .state('app.emp-detail', {
        url: '/emp-detail/:issue_id',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/technician/emp-detail.html',
            controller: 'detail_to_main'
          }
        }
      })
      .state('app.tech_person', {
        url: '/tech_person/:issue_id',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/technician/tech_person.html',
            controller: 'work_start_detail'
          }
        }
      })
      .state('app.work', {
        url: '/work',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/technician/work.html',
            controller: 'work_start'
          }
        }
      })
      .state('app.tech_issues', {
        url: '/tech_issues',
        views: {
          'menuContent': {
            templateUrl: 'templates/tech_issues.html',
            controller: 'TechCtrl'
          }
        }
      })

      .state('app.welcome', {
        url: '/welcome',
        cache: false,
        controller: 'AppCtrl',
        views: {
          'menuContent': {
            templateUrl: 'templates/welcome.html',
            controller: 'MyCtrl'
          }
        }
      })
      .state('app.forgot_password', {
        url: '/forgot_password',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/forgot_password.html',
            controller: 'forgotPasswordCtrl'
          }
        }
      })
      .state('app.inspection', {
        url: '/inspection',
        views: {
          'menuContent': {
            templateUrl: 'templates/property_head/inspection.html',
            controller: 'inspection_ctrl'
          }
        }
      })
      .state('app.manage_details', {
        url: '/manage_details/:id',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/director/manage_details.html',
            controller: 'ManageCtrl'
          }
        }
      })
      .state('app.tenant_list', {
        url: '/tenant_list',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/director/tenant_list.html',
            controller: 'tenant_list'
          }
        }
      })
      .state('app.manage_issue', {
        url: '/manage_issue',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/director/manage_issue.html',
            controller: 'ManagerissueCtrl'
          }
        }
      })
        .state('app.attendance', {
        url: '/attendance',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/attendance.html',
            controller: 'AttendanceCtrl'
          }
        }
      })
      .state('app.cust_detail', {
        url: '/cust_detail',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/director/cust_detail.html',
            controller: 'CustomeListCtrl'
          }
        }
      })
      .state('app.cus_info', {
        url: '/cus_info',
        params:{customer_id:null},
        cache:false,
        views: {
          'menuContent': {
            templateUrl: 'templates/cus_info.html',
            controller: 'CustomerInfoCtrl'
          }
        }
      })
           .state('app.notifi_details', {
        url: '/notifi_details',
        cache:false,
        views: {
          'menuContent': {
            templateUrl: 'templates/notifi_details.html',
             controller: 'AppCtrl'
          }
        }
      })
              .state('app.create_new', {
        url: '/create_new',
        views: {
          'menuContent': {
            templateUrl: 'templates/property_head/create_new.html'

          }
        }
      })
                .state('app.complete_job', {
        url: '/complete_job',
        views: {
          'menuContent': {
            templateUrl: 'templates/technician/complete_job.html',
          }
        }
      })
         .state('app.inspection_list', {
        url: '/inspection_list',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/director/inspection_list.html',
          controller: 'InsCtrl'

          }
        }
      })
           .state('app.ins_modal', {
        url: '/ins_modal',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/director/ins_modal.html',

          controller: 'InsCtrl'

          }
        }
      })
      .state('app.home_page', {
        url: '/home_page',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/home_page.html',
            controller: 'home_ctrl'
          }
        }
      });
    if (localStorage.getItem('rapro') != null) {
      user = JSON.parse(localStorage.getItem('rapro'));
      if ((user.admin_role_id == 4) || (user.admin_role_id == 7)) {
        $urlRouterProvider.otherwise('/app/welcome');
      } else {
        $urlRouterProvider.otherwise('/app/home_page');
      }
    } else {
      $urlRouterProvider.otherwise('/menu_welcome');
    }
  })
  .directive('numbersOnly', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function(inputValue) {
                if (inputValue == undefined) return ''
                var onlyNumeric = inputValue.replace(/[^0-9]/g, '');
                if (onlyNumeric != inputValue) {
                    modelCtrl.$setViewValue(onlyNumeric);
                    modelCtrl.$render();
                }
                return onlyNumeric;
            });
        }
    };
   })
