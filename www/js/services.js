var myApp = angular.module('starter.services', [])
  .factory('ajax_service', function ($http) {
    return {
      post_obj: function (url, data) {
        return $http.post(url, data).then(function (result) {
          // console.log(result);
          return result.data;
        }, function (error) {
          console.log(error);
        });
      },
      get_obj: function (url) {
        return $http.get(url).then(function (result) {
          return result.data;
        }, function (error) {
          console.log(error);
        });
      }
    };
  })
  .factory('Camera', function ($q) {
    return {
      getPicture: function (options) {
        var q = $q.defer();
        navigator.camera.getPicture(function (result) {
          q.resolve(result);
        }, function (err) {
          q.reject(err);
        }, options);
        return q.promise;
      }
    };
  })
  .factory('master_data', function (ajax_service) {
    return {
      home_data: function (user_data) {
        user_data.token = token;
        ajax_service.post_obj(url + 'home_page_data', user_data).then(function (result) {
          console.log(result);
          if (result.status == "success") {
            return result.data;
          }
        });
      },
      categories: function () {
        data = {};
        data.token = token;
        return ajax_service.post_obj(url + 'issue_categories', data).then(function (result) {
          if (result.status == "success") {
            return result.data;
          }
        });
      },
      get_obj: function (url) {
        return $http.get(url).then(function (result) {
          return result.data;
        }, function (error) {
          console.log(error);
        });
      }
    };
  })
  .factory('fb', function ($firebaseStorage) {
    return {
      push: function (url, data, d) {
        // alert(JSON.stringify(url, data, d));
        firebase.database().ref(url).push(data).then(function (res) {
          console.log(res);
          return res;
        });
      },
      set: function (url, data) {
        firebase.database().ref(url).set(data).then(function (res) {
          console.log(res);
          return res;
        });
      },
      storage_put: function (path, file) {
        // alert(path);
        // alert(JSON.stringify(file));
        var storageRef = firebase.storage().ref("rapro/" + path+'/test.jpg');
        var metadata = {
          contentType: file.type,
          md5Hash: true
        };
        fb_storage = $firebaseStorage(storageRef);
       uploadTask = fb_storage.$put(file, metadata);
       uploadTask.$complete(function (snapshot) {
          return snapshot.downloadURL;
        });
        // return fb_storage.$getDownloadURL().then(function (url) {
        // alert(JSON.stringify(url));
        //   return url;
        // });
      },
      };
  })
  .factory('location', function ( $cordovaGeolocation,$filter) {

    return {
          get_coords: function () {
            var posOptions = {
              timeout: 50000,
              maximumAge: 60000,
              enableHighAccuracy: true
            };
            return $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {

              var lat = position.coords.latitude;
              var lng = position.coords.longitude;


              data = {};
              data.lat = lat;
              data.lng = lng;
              data.timestamp = position.timestamp;
              date = $filter('date')(new Date(), 'dd-MM-yyyy');
              return {
                status:"success",
                data: data,
                date: date
              };
            }, function (err) {
              return {
                status:'error',
                msg: err.message
              };
            });
          },
          watch_coords: function () {
            var posOptions = {
              timeout: 50000,
              enableHighAccuracy: true
            };
            watch = $cordovaGeolocation.watchPosition(posOptions);
            return watch.then(function (position) {
              console.log(position);
              var lat = position.coords.latitude;
              var lng = position.coords.longitude;
              data = {};
              data.lat = lat;
              data.lng = lng;
              data.timestamp = position.timestamp;
              date = $filter('date')(new Date(), 'dd-MM-yyyy');
              return {
                data: data,
                date: date
              };
            });
          },
        };
  })

  .factory('acepush', function (ajax_service) {
    return {
      notification: function (obj) {
        ajax_service.post_obj('http://push.mylaporetoday.in/device_register', obj).then(function (result) {
          if (result.status == "success") {} else if (result.status == "error") {
            console.log(result.message);
          }
        }, function (error) {
          if (error == 500 || error == 404 || error == 0) {
            console.log("Push Api" + error);
          }
        });
      },
    };
  })
  .factory('camera_actions', function ($cordovaCamera, $cordovaFileTransfer) {
    return {
      selImages: function (type) {
        var options = {
          quality: 90,
          destinationType: Camera.DestinationType.FILE_URI,
          allowEdit: false,
          encodingType: Camera.EncodingType.PNG,
          targetWidth: 900,
          targetHeight: 900,
          popoverOptions: CameraPopoverOptions,
          saveToPhotoAlbum: false,
          correctOrientation: true
        };
        if (type == 'camera') {
          options.sourceType = Camera.PictureSourceType.CAMERA;
        }
        else if (type == 'file') {
          options.sourceType = Camera.PictureSourceType.PHOTOLIBRARY;
        }
        // alert(JSON.stringify(options));
        return $cordovaCamera.getPicture(options).then(function (imageUri) {
          return imageUri;
        }, function (err) {
          // error
        });

      },

      file_upload: function (server_url, file, params) {
          var options = {
          fileKey: "file",
          fileName: file,
          chunkedMode: false,
          // mimeType: "multipart/form-data",
          params:params,
        };
        if(params.type == 'videos'){
          options.mimeType = "video/mpeg";
        } else if (params.type == 'photos'){
          options.mimeType = "image/jpeg";
        }
      return $cordovaFileTransfer.upload(server_url, file, options).then(function (response) {
          // return response.response;

       return {
            status:'success',
            response:response.response
          };
        }, function (err) {
       return {
            status:'error',
            msg: err.message
          };
          // alert('err'+JSON.stringify(err));
        });
      }
    };
  })
  .factory('modal_service', function ($ionicModal) {
 return {
 from_url: function (templateUrl,options) {
        return $ionicModal.fromTemplateUrl(templateUrl, options).then(function (modal) {
          return modal;
          });
 },
  from_template: function (template, options) {
 return $ionicModal.fromTemplate(template, options).then(function (modal) {
   return modal;
 });
  },
 };
  })

.factory('ConnectivityMonitor', function($rootScope,  $ionicPopup,  $cordovaNetwork,$timeout){

  return {

    startWatching: function(){
      if(ionic.Platform.isWebView()){


               $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
                 var myPopup =  $ionicPopup.alert({
                          title: 'No Internet Connectivity',
                          template: '<center  class="text-center">Please Turn On The Internet</center>',
                          buttons: [{ text: 'ok' ,
                        type: 'button-assertive'}]
                        });
                        // $timeout(function() {
                        //       myPopup.close(); //close the popup after 3 seconds for some reason
                        //    }, 3000);
                 console.log("went offline");
               });

             }

}
  }
})


;
